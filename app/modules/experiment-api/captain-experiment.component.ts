// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter
} from 'angular2/core';

import {KeyValuePair} from "../key-value-pair.class";
import {CaptainTrial} from '../trial-api/captain-trial.component';
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialBlockStore} from "../trial-block-store/trial-block-store.service";
import {
	ExperimentDefinition,
	ExperimentSection
} from "./experiment-definition.interface";
import {CSVResultService} from "../result-services/csv-result.service";
import {EmuDBResultService} from "../result-services/emudb-result.service";


@Component({
	selector: 'captain-experiment',
	template: `
		<div class="form-horizontal container-fluid" *ngIf="sectionIndex < 0">
			<div class="form-group">
				<h1 class="col-sm-12">Subject/session information</h1>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-3">Experiment name</label>
				<div class="col-sm-9">
					<input class="form-control" [value]="data.name" 
						   disabled> 
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-3">Subject ID</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="Subject ID" 
					[(ngModel)]="subjectID">
				</div>
			</div>
						
			<div class="form-group">
				<label class="control-label col-sm-3">EmuDB server</label>
				<div class="col-sm-9">
					<input class="form-control" placeholder="EmuDB server" 
					[(ngModel)]="emuDBServer">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-12">&nbsp;</label>
			</div>
			
			<div class="form-group">
				<label class="col-sm-12">Subject metadata</label>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3">Key</label>
				<label class="col-sm-8">Value</label>
				<label class="col-sm-1"></label>
			</div>
			
			<div class="form-group" *ngFor="#info of subjectMetadata; #i=index">
					<div class="col-sm-3">
						<input class="form-control" [(ngModel)]="info.key">
					</div>
					<div class="col-sm-8">
						<input class="form-control" [(ngModel)]="info.value">
					</div>
					<div class="col-sm-1">
						<button (click)="removeMetadata(i)"
								class="btn btn-default">
							<span class="glyphicon glyphicon-minus"></span>
						</button>
					</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-11"></label>
				<div class="col-sm-1">
					<button (click)="extendMetadata()" class="btn btn-default">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</div>
			</div>
			
			<button class="btn btn-default" (click)="startExperiment()">
				Start experiment
			</button>
		</div>
		
		<captain-trial
			*ngIf="sectionIndex >=0"
			(result)="currentTrialBlock.updateResult($event)"
			(finish)="
				currentTrial = [];
				currentTrialBlock.finishTrial(); 
				nextTrial();
			"
			[elements]="currentTrial">
		</captain-trial>
		`,
	styles: [`
		:host captain-trial {
			width: 100%;
			height: 100%;
		    display: flex;
		    flex-direction: column;
		    
		    font-size: x-large;
		}
		
		:host captain-trial > * {
			flex-grow: 1;
			text-align: center;
			padding: 10px;
			margin: 10px;
			
			display: flex;
    		flex-direction: column;
    		justify-content: center;
		}

		:host captain-trial .component-label {
			display: none;
		}
	`],
	directives: [CaptainTrial],
	providers: []
})
export class CaptainExperiment implements OnInit {
	private error;

	constructor(private _csvResultService:CSVResultService,
	            private _emuDBResultService:EmuDBResultService,
	            private _trialBlockStore:TrialBlockStore) {
		var browserStoredURL = localStorage.getItem('emuDBResultServerURL');

		if (browserStoredURL) {
			this.emuDBServer = browserStoredURL;
		} else {
			this.emuDBServer = 'wss://localhost:17890/some-database?authToken=some-token';
		}
	}

	//////////
	// Subject data
	//

	private subjectID:string = "";
	private subjectMetadata:KeyValuePair<string>[] = [];

	private emuDBServer:string = '';

	//
	//////////

	//////////
	// Track the flow through the experiment (section -> trial block -> trial)
	//

	@Output() error:EventEmitter<any> = new EventEmitter<any>();

	@Output() end:EventEmitter<void> = new EventEmitter<void>();

	@Input() data:ExperimentDefinition;

	// The experiment has a list of sections and the sections are always
	// executed in the order they are defined there
	private sectionIndex:number = -1;

	// Every section has a list of trial blocks and a setting whether the trial
	// blocks shall be executed in pre-defined or random order
	private trialBlockList:{type:string, stimulusObject:KeyValuePair<any>[]}[] = [];
	private trialBlockIndex:number = -1;
	private currentTrialBlock:TrialBlock;

	// Every trial block has a controller class and that class returns
	// trials as long as it pleases
	private currentTrial:TrialElementDefinition<any>[] = [];

	//
	//////////

	//////////
	// Control the flow through the experiment (section -> trial block -> trial)
	//
	// Note that some of the flow control (ending a trial) is in the template
	//

	private nextTrial() {
		this.currentTrialBlock.getNextTrial()
			.then((trial) => {
				this.currentTrial = trial;

				if (this.currentTrial === null) {
					this.nextTrialBlock();
				}
			})
			.catch((reason) => {
				// When loading a trial fails, we have to abort the current
				// trial block:
				// Throw error and proceed to next trial block
				this.error.emit({
					message: 'Could not load trial in trial block ' + this.trialBlockIndex + ' in section ' + this.sectionIndex,
					action: 'nextTrial',
					previousError: reason
				});
				this.nextTrialBlock();
			});
	}

	private nextTrialBlock():void {
		this.trialBlockIndex++;

		if (this.trialBlockIndex >= this.trialBlockList.length) {
			this.nextSection();
			return;
		}

		this.currentTrialBlock = this._trialBlockStore.getTrialBlockInstance(
			this.trialBlockList[this.trialBlockIndex].type
		);
		this.currentTrialBlock.init(
			this.trialBlockList[this.trialBlockIndex].stimulusObject
		).then(() => {
			this.nextTrial();
		}).catch((reason) => {
			// When loading a trial block fails, we skip it:
			// Throw error and proceed to next trial block
			this.error.emit({
				message: 'Failed to initialise trial block ' + this.trialBlockIndex + ' in section ' + this.sectionIndex,
				action: 'nextTrialBlock',
				previousError: reason
			});
			this.nextTrialBlock();
		});
	}

	private nextSection() {
		++this.sectionIndex;

		if (this.sectionIndex >= this.data.sections.length) {
			this.endExperiment();
			return;
		}

		// Prepare this section's list of trial blocks and randomise it if
		// so requested
		this.trialBlockIndex = -1;
		this.trialBlockList = [];

		var sectionData:ExperimentSection = this.data.sections[this.sectionIndex];

		for (let i = 0; i < sectionData.trialBlocks.length; ++i) {
			for (let j = 0; j < sectionData.trialBlocks[i].repetitions; ++j) {
				for (let k = 0; k < sectionData.trialBlocks[i].stimulusObjects.length; ++k) {
					this.trialBlockList.push({
						type: sectionData.trialBlocks[i].type,
						stimulusObject: sectionData.trialBlocks[i].stimulusObjects[k]
					});
				}
			}
		}

		if (sectionData.randomise) {
			this.shuffle(this.trialBlockList);
		}

		this.nextTrialBlock();
	}

	private endExperiment():void {
		this.end.emit(undefined);
	}

	private startExperiment():void {
		// Prepare result services
		this._csvResultService.setSubject(this.subjectID);
		localStorage.setItem('emuDBResultServerURL', this.emuDBServer);
		this._emuDBResultService.init(this.emuDBServer);
		this._emuDBResultService.setSubject(this.subjectID, this.subjectMetadata);

		// Start progression through sections, which will in turn start the
		// progression through trial blocks and trials
		this.nextSection();
	}

	//
	//////////

	//////////
	// Helper functions
	//

	/**
	 * Randomise an array.
	 *
	 * @todo statistically test this implementation
	 *
	 * Props to CoolAJ86 on StackOverflow.
	 * http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array#2450976
	 *
	 * @param array
	 * @returns {any}
	 */
	private shuffle(array) {
		var currentIndex = array.length;
		var temporaryValue;
		var randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

	//
	//////////

	/// Let the user edit subject metadata
	private removeMetadata(index:number):void {
		this.subjectMetadata.splice(index, 1);
	}

	/// Let the user edit subject metadata
	private extendMetadata():void {
		this.subjectMetadata.push({key: '', value: ''});
	}

	ngOnInit():any {
		this.subjectMetadata.push({key: 'startTime', value: Date()});
	}
}
