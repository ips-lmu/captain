// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component} from "angular2/core";
import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

import {CaptainTrialElement} from "../captain-trial-element.class";
import {TrialElementDefinition} from "../trial-element-definition.interface";

@Component({
	selector: 'captain-button-list',
	template: `
		<button *ngFor="#label of element.value"
		        (click)="result.emit(label); finish.emit();">
			{{label}}
		</button>
		<p class="component-label">buttonList</p>
	`,
	styles: [`
		button { margin: 0 30px;}
	`]
})
export class CaptainButtonList extends CaptainTrialElement {
	@Input() element:TrialElementDefinition<string[]>;
	@Output() result = new EventEmitter<string>();
	@Output() finish:EventEmitter<void> = new EventEmitter<void>();
}
