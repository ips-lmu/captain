import {Component} from "angular2/core";


@Component({
	selector: 'captain-info',
	directives: [],
	template: `
<h1>Welcome!</h1>
<p class="sub-heading">To the Captain online demo</p>
<p>
Captain is a research tool designed for the scientific evaluation of methods in
computer-assisted pronunciation training (CAPT). The idea was formed at a workshop
organised by Saarland University (see 
<a href="http://www.ifcasl.org/feedback_workshop.html">IFCASL</a>).
</p>

<p>
The tool is a work-in-progress, but has successfully been used in one pilot study
(Jochim & Draxler, 2016) to evaluate an automatic accent correction method.
</p>

<p>
The major goals for Captain are
</p>

<ol>
<li>to re-use participant responses as stimuli in later trials, without
manual intervention and</li>
<li>to employ complex signal processing methods at the time of conducting the 
experiment, without manual intervention.</li>
</ol>

<h2>Demo</h2>

<p>
Clicking on “Labor” (German for laboratory) to the left will lead you to a list
of pre-defined experiments. As a start, you will find there the experiment 
from Jochim &amp; Draxler (2016). You can go try the experiment yourself, but the
results will not be saved. 
</p>

<p>
The “Werkstatt” (workshop) is the area where new experiments can be compiled.
This area, however, is only partly functional. 
</p>

<h2>Source Code</h2>

<p>
Captain’s source code is available at <a href="https://gitlab.lrz.de/ips-lmu/captain">https://gitlab.lrz.de/ips-lmu/captain</a>.
</p>

<h2>References</h2>

<p>
Jochim, M. & Draxler, C. (2016). Fully Automated Accent Correction for 
Computer-Assisted Speech Rhythm Training. In <i>Proceedings P&P12</i>.
</p>
	`,
	styles: [`
		:host(.experimentRunning) {
			background-color: white;
			position: absolute;
		    width: 100%;
		    height: 100%;
		    top: 0;
		    left: 0;
		}
		
		.sub-heading {
			padding-bottom: 10px;
			border-bottom: 1px solid grey;
		}
	`]
})
export class CaptainInfo {

}
