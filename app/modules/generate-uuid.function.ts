// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

// @todo credit those uuid guys
/**
 * @todo document this
 * @param a
 * @returns {string}
 */
export function generateUUID(a?) {
	return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ((1e7).toString() + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, generateUUID)
};
