// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Injectable} from "angular2/core";

import {KeyValuePair} from "../key-value-pair.class";
import {
	EmuDBBundleAnnotation,
	EmuDBLevel,
	EmuDBLink
} from "./emudb-bundle-annotation.interface";
import {
	EMUWebappProtocolConnection,
	EmuDBBundleInTransit
} from "./emu-webapp-protocol";
import {
	arrayBufferToBase64,
	audioBufferToWav
} from "browser-signal-processing/browser-signal-processing";

/**
 * Stores subjects and results of an experiment and exports them in emuDB
 * format.
 *
 * When an experiment is started with a new subject, this class expects a
 * subject identifier to be passed via setSubject(). That function also
 * accepts metadata describing the subject. For storing the results, a simple
 * API and a more comprehensive one are offered.
 *
 * An emuDB is organised into sessions and bundles. This class creates a
 * session for each subject ID.
 *
 * The simple API does not require any knowledge of the emuDB format on the
 * user's side, but consequently cannot completely exploit the format's
 * power. It only comprises the functions getNewBundleNamePrefix() and
 * addSimpleBundle().
 *
 * The comprehensive API requires that the user be familiar with emuDB
 * internals and aims to exploit the format's features fairly completely.
 *
 * @todo [CompAPI] Specify and implement comprehensive API
 */
@Injectable()
export class EmuDBResultService {
	/**
	 * @todo document this here and in the file's doc
	 */
	public init(serverURL:string) {
		this.serverConnection = new EMUWebappProtocolConnection();

		this.serverConnection.addEventListener('error', (event) => {
			console.debug('Received error event from WS connection', event);
		});

		this.serverConnection.init(serverURL).then(() => {
			return this.serverConnection.getProtocol().then((value) => {
				return this.serverConnection.getDoUserManagement().then((value) => {
					return this.serverConnection.getDBconfigFile().then((value) => {
						console.log('Connected to emu server');
					});
				});
			});
		}).catch((error) => {
			console.log('Connection to emu server failed', error);
		});
	}

	/**
	 * Provide an identifier for a subject. All results submitted afterwards
	 * will be assigned to that subject (until a new subject ID is provided).
	 *
	 * All parameters are copied before storing.
	 *
	 * @param {string} id Subject identifier (free-form string)
	 * @param {KeyValuePair<string>[]} metadata List of key-value pairs to
	 * describe
	 * the subject
	 */
	public setSubject(id:string, metadata?:KeyValuePair<string>[]):void {
		this.subjectID = id;
		this.prefixCounter = 0;

		// See if a session has already been created with the current subjectID
		// If not, create one.
		var session:KeyValuePair<KeyValuePair<any>>;
		session = KeyValuePair.findKey(this.sessions, id);
		if (session === undefined) {
			this.sessions.push({
				key: id,
				value: []
			});
		}

		if (metadata === undefined) {
			this.subjectMetadata = [];
		} else {
			this.subjectMetadata = KeyValuePair.copyList(metadata);
		}
	}

	/**
	 * Returns a unique name (or unique name prefix) for the next bundle (or
	 * set of bundles).
	 *
	 * Increments an internal counter of used numbers and returns the next
	 * available number as a four-figure string.
	 *
	 * @returns {string} A number (as a four-figure string) to be used as a
	 * bundle name
	 */
	public getNewBundleNamePrefix():string {
		var prefix:string;

		// Create new prefix until one is found that is not consumed
		do {
			++this.prefixCounter;
			prefix = this.prefixCounter.toString();

			if (prefix.length === 1) {
				prefix = "000" + prefix;
			} else if (prefix.length === 2) {
				prefix = "00" + prefix;
			} else if (prefix.length === 3) {
				prefix = "0" + prefix;
			}
		} while (this.isPrefixConsumed(prefix));

		return prefix;
	}

	/**
	 * Check all existing bundles' names to see if they start with the given
	 * prefix. If at least one does, the prefix is deemed as "consumed".
	 *
	 * @param {string} prefix The prefix to check.
	 * @returns {boolean} Whether {prefix} is consumed.
	 */
	private isPrefixConsumed(prefix:string):boolean {
		// @todo [reuse-session] Implement isPrefixConsumed()
		return false;
	}


	/**
	 * Simple API to construct an emuDB bundleAnnotation from one set of results.
	 *
	 * Accepts a signal and a list of key-value pairs to annotate that
	 * signal. Providing a unique name for the bundleAnnotation is mandatory. The
	 * helper function getNewBundleNamePrefix() can be used to construct one.
	 *
	 * This function generates two emuDB levels, Uterrance and Subject, to
	 * annotate the bundle. On the Utterance level, all information from the
	 * annotation object passed into this function are stored. On the
	 * Subject level, the information passed into the service via
	 * setSubject() are stored. Both levels contain one item each and use
	 * parallel labels to accomadate the individual key value pairs.
	 * Utterance is above Subject in the hierarchy.
	 *
	 * Optionally, pre-defined level and link objects can be passed into
	 * this function. They will be put in the annotation object. Via the
	 * parameter `linkToSuperLevels`, one or more of the level objects
	 * passed in can be chosen to be connected to the Subject level. In that
	 * case, all its items are connected to the only item on the Subject level.
	 *
	 * @todo is parallel labels the term used in the docs?     *
	 *
	 * All parameters are copied before storing.
	 *
	 * @todo [CompAPI] Define type for signal and handle signal+sampleRate
	 *
	 * @param {string} name A unique name for the bundleAnnotation
	 * @param {KeyValuePair[]} annotation A list of key-value pairs to
	 *                                     annotate {signal}
	 * @param {AudioBuffer} signal A mono signal to be saved as WAV within the
	 *                               bundleAnnotation.
	 * @param {EmuDBLevel[]} levels A list of pre-defined level objects to
	 *                               add to the bundle annotion.
	 * @param {EmuDBLink[]} links A list of pre-defined link objects to add
	 *                             to the bundle annotation.
	 * @param {string[]} linkToSuperLevels Names of levels whose items will
	 *                                       be connected to the Subject level.
	 * @returns {boolean} Whether the bundleAnnotation has been added or rejected due
	 *                     to a name collision
	 */
	public addSimpleBundle(name:string,
	                       annotation:KeyValuePair<any>[],
	                       signal?:AudioBuffer,
	                       levels?:EmuDBLevel[],
	                       links?:EmuDBLink[],
	                       linkToSuperLevels?:string[]):boolean {
		//////////
		// Check if name is already used
		//
		var session:KeyValuePair<EmuDBBundleInTransit[]>;
		session = KeyValuePair.findKey(this.sessions, this.subjectID);
		// @todo [reuse-session] check if bundle name is already taken
		//       and if so, return false

		//////////
		// Prepare bundleAnnotation template
		//
		var bundleAnnotation:EmuDBBundleAnnotation = {
			name: name,
			annotates: "",
			sampleRate: 0,
			levels: [{
				name: "Utterance",
				type: "ITEM",
				items: [{
					id: 1,
					labels: [{
						name: "Utterance",
						value: ""
					}]
				}]
			}, {
				name: "Subject",
				type: "ITEM",
				items: [{
					id: 2,
					labels: [{
						name: "Subject",
						value: this.subjectID
					}]
				}]
			}],
			links: [{
				fromID: 1,
				toID: 2
			}]
		};

		//////////
		// Add signal information if signal was supplied
		//
		if (signal instanceof AudioBuffer) {
			bundleAnnotation.sampleRate = signal.sampleRate;
			bundleAnnotation.annotates = name + '.' + this.mediaFileExtension;
		}

		//////////
		// Copy annotations into bundleAnnotation (Utterance level)
		//
		var annotationCopy = KeyValuePair.copyList(annotation);
		for (let i = 0; i < annotationCopy.length; ++i) {
			if (annotationCopy[i].value) {
				bundleAnnotation.levels[0].items[0].labels.push({
					name: annotationCopy[i].key,
					value: annotationCopy[i].value.toString()
				});
			} else {
				bundleAnnotation.levels[0].items[0].labels.push({
					name: annotationCopy[i].key,
					value: ""
				});
			}
		}

		//////////
		// Copy subject metadata into bundleAnnotation (Subject level)
		//
		var subjectMetadataCopy = KeyValuePair.copyList(this.subjectMetadata);
		for (let i = 0; i < subjectMetadataCopy.length; ++i) {
			if (subjectMetadataCopy[i].value) {
				bundleAnnotation.levels[1].items[0].labels.push({
					name: subjectMetadataCopy[i].key,
					value: subjectMetadataCopy[i].value.toString()
				});
			} else {
				bundleAnnotation.levels[1].items[0].labels.push({
					name: subjectMetadataCopy[i].key,
					value: ""
				});
			}
		}

		//////////
		// Add pre-defined levels and links to bundleAnnotation
		//
		bundleAnnotation.levels = bundleAnnotation.levels.concat(
			Object.assign([], levels)
		);
		bundleAnnotation.links = bundleAnnotation.links.concat(
			Object.assign([], links)
		);

		//////////
		// Connect levels passed in to the levels generated within this function
		//
		if (Array.isArray(linkToSuperLevels) && Array.isArray(levels)) {
			for (let i = 0; i < linkToSuperLevels.length; ++i) {
				for (let j = 0; j < levels.length; ++j) {
					if (levels[j].name === linkToSuperLevels[i]) {
						for (let k = 0; k < levels[j].items.length; ++k) {
							bundleAnnotation.links.push({
								fromID: 2,
								toID: levels[j].items[k].id
							});
						}
					}
				}
			}
		}

		//////////
		// Combine annotations and signal
		//
		var bundleData:EmuDBBundleInTransit = {
			session: session.key,
			annotation: bundleAnnotation
		};

		if (signal instanceof AudioBuffer) {
			var wave = audioBufferToWav(signal);

			bundleData.mediaFile = {
				encoding: 'BASE64',
				data: arrayBufferToBase64(wave)
			}
		}

		//////////
		// Adapt DBconfig
		//
		// @todo adapt DBconfig

		//////////
		// Save bundle
		//
		session.value.push(bundleData);
		this.serverConnection.saveBundle(bundleData).then((value) => {
			console.log('Successfully stored emuDB bundle on server', bundleData);
		}).catch((reason) => {
			console.log('Error while storing emuDB bundle on server:', reason, bundleData);
		});

		return true;
	}


	/**
	 * This is a debug-only function
	 * @returns {Object}
	 */
	public getSessions():KeyValuePair<EmuDBBundleInTransit[]>[] {
		return this.sessions;
	}

	//////////
	// Private fields
	//

	// File name extension for recordings
	private mediaFileExtension = "wav";

	// Identify and describe current subject
	private subjectID:string = "";
	private subjectMetadata:KeyValuePair<string>[] = [];

	// This is used to generate bundle name prefixes automatically
	private prefixCounter:number = 0;

	// Hold data
	private sessions:KeyValuePair<EmuDBBundleInTransit[]>[] = [];

	private serverConnection:EMUWebappProtocolConnection;
}
