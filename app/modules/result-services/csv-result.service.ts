// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Injectable} from "angular2/core";
import {KeyValuePair} from "../key-value-pair.class";

/**
 * Stores experimental results in CSV format.
 *
 * The API is quite simple, comprising the functions setSubject() and
 * addObservation().
 *
 * When an experiment is started with a new subject, this class expects a
 * subject identifier to be passed via setSubject(). That function also
 * accepts metadata describing the subject. Results can be stored via
 * addObservation().
 *
 * @author Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
 */
@Injectable()
export class CSVResultService {
	/**
	 * Store an observation. The observation is a list of key-value pairs
	 * with arbitrar keys.
	 *
	 * Before storing, the observation is merged with the metadata of the
	 * current subject.
	 *
	 * All parameters (including the merged metadata) are copied before storing.
	 *
	 * @todo WARNING If the key sets of metadata and observation are not
	 *       disjoint, the behaviour is undefined.
	 *
	 * @param {KeyValuePair[]} observation The observation to store.
	 */
	public addObservation(observation:KeyValuePair<any>[]):void {
		var csvRow:KeyValuePair<any>[] = KeyValuePair.copyList(observation);

		// Write subjectID into observation
		csvRow.push({
			key: 'subjectID',
			value: this.subjectID
		});

		// Merge metadata with observation
		csvRow = csvRow.concat(
			KeyValuePair.copyList(this.subjectMetadata)
		);

		this.resultSet.push(csvRow);
	}

	/**
	 * Provide an identifier for a subject. All results submitted afterwards
	 * will be assigned to that subject (until a new subject ID is provided).
	 *
	 * All parameters are copied before storing.
	 *
	 * @param {string} id Subject identifier (free-form string)
	 * @param {KeyValuePair<string>[]} metadata List of key-value pairs to
	 *                                            describe the subject
	 */
	public setSubject(id:string, metadata?:KeyValuePair<string>[]):void {
		this.subjectID = id;

		if (metadata === undefined) {
			this.subjectMetadata = [];
		} else {
			this.subjectMetadata = KeyValuePair.copyList(metadata);
		}
	}

	/**
	 * Debug function to retrieve results
	 * @returns {string[][]}
	 */
	public getResults():string[][] {
		var csvHeader:string[] = this.enumerateFields();
		var csv:string[][] = [csvHeader];

		for (var i = 0; i < this.resultSet.length; ++i) {
			var row:string[] = [];
			for (var j = 0; j < csvHeader.length; ++j) {
				var pair:KeyValuePair<any> =
					KeyValuePair.findKey(this.resultSet[i], csvHeader[j]);

				if (pair === undefined) {
					row.push(undefined);
				} else {
					row.push(pair.value);
				}
			}
			csv.push(row);
		}
		return csv;
	}

	/**
	 * Return a joint list of all keys present in any of the stored
	 * observations. The can be used as CSV header.
	 *
	 * Since the observations are stored with this.subjectMetadata before
	 * storing, this includes all keys present in metadata as well.
	 *
	 * @returns {string[]} List of keys present
	 */
	private enumerateFields():string[] {
		var fields:string[] = [];

		// We prefill the field list with subjectID. It would be found anyway
		// during the loop below, but this ensures that the field comes first.
		fields.push('subjectID');

		// Search all results for new keys
		for (let i = 0; i < this.resultSet.length; ++i) {
			for (let j = 0; j < this.resultSet[i].length; ++j) {
				if (fields.indexOf(this.resultSet[i][j].key) === -1) {
					fields.push(this.resultSet[i][j].key);
				}
			}
		}

		return fields;
	}


	//////////
	// Private fields
	//
	private resultSet:KeyValuePair<any>[][] = [];

	private subjectID:string = "";
	private subjectMetadata:KeyValuePair<string>[] = [];
}
