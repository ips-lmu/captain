import {ExperimentDefinition} from "../../modules/experiment-api/experiment-definition.interface";

export var speechRhythmTrainer:ExperimentDefinition = {
	version: '',
	uuid: '286b06c4-a662-44be-b55a-4557a893dce0',
	name: 'speech-rhythm-trainer',

	sections: [{
		randomise: false,

		trialBlocks: [{
			type: 'Description',
			repetitions: 1,
			stimulusObjects: [
[{key: 'text', value: 'Willkommen bei der Demo zum Sprechrhythmus-Trainer!' +
' Hier kommen zehn deutsche Sätze, um das System auszuprobieren.'}, {key: 'button', value: 'Weiter'}]
			]
		}]
	}, {
		randomise: true,
		trialBlocks: [{
			type: 'PsolaFeedback',
			repetitions: 1,

			stimulusObjects: [
[{key:'text', value:'Jetzt sitzen sie beim Frühstück.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-009.wav'}],
[{key:'text', value:'Es ist acht Uhr morgens.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-010.wav'}],
[{key:'text', value:'Vater hat den Tisch gedeckt.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-011.wav'}],
[{key:'text', value:'Günther muss noch einkaufen gehen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-021.wav'}],
[{key:'text', value:'Seine Frau macht ein trauriges Gesicht.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-036.wav'}],
[{key:'text', value:'Du solltest weniger rauchen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-037.wav'}],
[{key:'text', value:'Was macht denn Dein verstauchter Fuß?'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-044.wav'}],
[{key:'text', value:'Zurück geht\'s mit der Bahn.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-056.wav'}],
[{key:'text', value:'Im Topf kocht das Wasser.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-066.wav'}],
[{key:'text', value:'Die drei Männer sind begeistert.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-073.wav'}]
			]
		}]
	}, {
		randomise: false,

		trialBlocks: [{
			type: 'Description',
			repetitions: 1,
			stimulusObjects: [
[{key: 'text', value: 'Fertig!'}, {key: 'button', value: ':-)'}]
			]
		}]
	}]
};
