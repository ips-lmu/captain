// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {ExperimentDefinition} from "../../modules/experiment-api/experiment-definition.interface";

export var demoExperiment:ExperimentDefinition = {
	version: 'alpha',
	uuid: '1e8a9a78-9817-458a-b02f-baaf8de9bb39',
	name: 'demo-experiment',

	sections: [{
		randomise: false, // Might be a randomisation type rather than a boolean (as it is in Praat)

		trialBlocks: [{
			type: 'ForcedChoice',
			repetitions: 1,
			stimulusObjects: [[
				{key: 'prompt', value: 'Sprechen Sie eher Dialekt oder eher' +
				' Hochdeutsch?'},
				{key: 'choice', value: ['Dialekt', 'Hochdeutsch']}
			]]
		}, {
			type: 'TestRecorder',
			repetitions: 1,
			stimulusObjects: [[
				{key: 'stimulus', value: 'Bitte sagen Sie „Holz“'},
				{key: 'recording', value: 'app/experiments/demo-experiment/nurhierundda.wav'}
			]]
		}]
	}]
};
