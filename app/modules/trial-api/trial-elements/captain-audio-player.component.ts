// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component, Input, ViewChild, Output, EventEmitter} from "angular2/core";

import {CaptainTrialElement} from "../captain-trial-element.class";
import {TrialElementDefinition} from "../trial-element-definition.interface";

/**
 * <captain-audio-player> is a trial element. As such, it allows an
 * "element" attribute and fires the events result and finish.
 *
 * element can have the following properties:
 *
 * - autoPlay: If true, automatically start playing back the audio stimulus.
 * - maxRepetitions: Number. As long as the stimulus has not been played
 *                    this many times, a button is shown to play it again.
 */
@Component({
	selector: 'captain-audio-player',
	template: `
		<p class="component-caption">{{_element.text}}</p>
		<p *ngIf="audioTagMode" >
			<audio #audioTag [src]="_element.value"></audio>
		</p>
		
		<p class="component-controls" *ngIf="playbackCounter < _element.maxRepetitions">
			<button (click)="play()">
				<span class="glyphicon glyphicon-play"></span>
			</button>
		</p>	
		
		<p class="component-label">audioPlayer</p>
	`
})
export class CaptainAudioPlayer extends CaptainTrialElement {
	@ViewChild('audioTag') audioTag;

	@Output() result:EventEmitter<any> = new EventEmitter<any>();

	@Input() set element(element:TrialElementDefinition<String|AudioBuffer>) {
		this._element = element;

		if (element.value instanceof AudioBuffer) {
			this.audioTagMode = false;
		} else {
			this.audioTagMode = true;
		}
		
		if (element.autoPlay) {
			this.play();
		}
	}

	// @todo when this is <string> rather than <String>, the typescript
	// compiler runs for ~13 seconds rather than ~1 sec - why is this so?
	private	_element:TrialElementDefinition<String|AudioBuffer>;

	/**
	 * The "audio tag mode" is enabled when we get passed a string as main
	 * value. This string is then treated as a URL an processed via an HTML
	 * <audio> tag. If the main value is not a string, it must be an audio
	 * signal.
	 * @type {boolean}
	 */
	private audioTagMode = false;

	/**
	 * Keep track of how often the stimulus has been played.
	 * @type {number}
	 */
	private playbackCounter:number = 0;

	/**
	 *
	 */
	play() {
		++this.playbackCounter;
		this.result.emit(this.playbackCounter);

		// Rather than
		//     if (value instanceof AudioBuffer)
		// this should be like this:
		//     if (this.audioTagMode)
		// But we need a type guard on value, which is why we have to
		// duplicate the instanceof check (the check is already done in the
		// element setter). Also, type guards currently (as of typescript 1.8.9)
		// only work with local variables, which is why we have to assign a
		// local variable (see typescript issue #5534).
		var value:String|AudioBuffer = this._element.value;
		if (value instanceof AudioBuffer) {
			// Play back the signal and make sure that our resources are
			// cleaned when playback is finished
			try {
				var context = new AudioContext();
			} catch (error) {
				console.log({
					message: 'Could not play audio file',
					action: 'CaptainAudioPlayer.play',
					previousError: error
				});
				return;
			}
			var node = context.createBufferSource();
			node.buffer = value;
			node.connect(context.destination);
			node.onended = (event) => {
				context.close();
			};
			node.start();
		} else {
			// Fire <audio> tag
			this.audioTag._appElement.nativeElement.play();
		}
	};
}
