// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component, Output, EventEmitter, Input} from 'angular2/core';

import {CaptainTrialElement} from "../captain-trial-element.class";
import {AudioRecorder} from "browser-signal-processing/browser-signal-processing";
import {TrialElementDefinition} from "../trial-element-definition.interface";

/**
 * <captain-audio-recorder> is a trial element. As such, it allows an
 * "element" attribute and fires the events result and finish.
 *
 * element can have the following properties:
 *
 * - recordButton: If true, a button to start recording is shown.
 * - stopButton: If true, a button to stop recording is shown.
 * - autoRecord: If 0, automatically start recording. If greater than 0,
 *                start recording after that many milliseconds. If not set
 *                or less than 0, do not start automatically.
 * - duration: If greater than 0, stop recording that many milliseconds
 *              after starting.
 * - silenceDetection: If greater than 0, stop recording once that many
 *                      silent milliseconds have been detected.
 * - proceedButton: If set, use the value as label for the finish button. If
 *                  not set, the trial is automatically finished when the
 *                  first recording ends.
 * - listRecordings: If true, show a button for each recording the
 *                    participant has made so they can be replayed. Only
 *                    makes sense if proceedButton is set (otherwise the
 *                    trial ends after the first recording and no time is
 *                    left to replay anything).
 * - sampleRate: Set the recording sample rate.
 * - bufferSize: Set the recording buffer size.
 *
 */
@Component({
	selector: 'captain-audio-recorder',
	template: `
		<div>
		    <p class="recording-state">
		        {{recordingState}}
		    </p>
		
			<p class="controls">
				<button *ngIf="_element?.recordButton" (click)="record()" [disabled]="recorder">
			        &#9899;
		        </button>
		        <button *ngIf="_element?.stopButton" (click)="stopRecording()" [disabled]="!recorder">
		            <span class="glyphicon glyphicon-stop"></span>
				</button>
		    </p>
		
			<div *ngIf="_element?.listRecordings">
			    <p *ngFor="#recording of recordings" class="recording">
	                <button (click)="play(recording)">
	                    <span class="glyphicon glyphicon-play"></span>
	                </button>
			    </p>
		    </div>
		    
		    <div *ngIf="_element.proceedButton && recordings.length > 0">
		        <button (click)="finish.emit()">
		            {{_element.proceedButton}}
		        </button> 
			</div>
		
			<p class="component-label">audioRecorder</p>
		</div>
	`
})
export class CaptainAudioRecorder extends CaptainTrialElement {
	// @todo when this is <string> rather than <String>, the typescript
	// compiler runs for ~13 seconds rather than ~1 sec - why is this so?
	private	_element:TrialElementDefinition<String|AudioBuffer>;

	private recorder:AudioRecorder;
	private recordingState = '';
	private recordings:AudioBuffer[] = [];

	@Output() result:EventEmitter<any> = new EventEmitter<any>();
	@Output() finish:EventEmitter<void> = new EventEmitter<void>();

	@Input() set element(element:TrialElementDefinition<String|AudioBuffer>) {
		this._element = element;

		// @todo is this function fired when the element is changed?
		// @todo which is best to trigger auto recording? ngOnInit or @Input element?

		this.autoStart();
	}

	/**
	 * Trigger autostart of recording if element.autoRecord is set properly.
	 */
	private autoStart () {
		if (this._element.autoRecord === 0) {
			this.record();
		} else if (typeof this._element.autoRecord === 'number' && this._element.autoRecord > 0) {
			setTimeout(() => {
				this.record();
			}, this._element.autoRecord);
		}
	}

	private play(signal) {
		try {
			var context = new AudioContext();
		} catch (error) {
			console.log({
				message: 'Could not play audio file',
				action: 'CaptainAudioRecorder.play',
				previousError: error
			});
			return;
		}
		var sourceNode = context.createBufferSource();
		sourceNode.buffer = signal;
		sourceNode.connect(context.destination);
		sourceNode.onended = () => {
			context.close();
		};
		sourceNode.start();
	};

	private stopRecording() {
		if (this.recorder) {
			this.recorder.stop();
		}
	};

	/**
	 * Start recording, making sure element properties are correctly
	 * implemented.
	 */
	private record() {
		if (this.recorder) {
			return;
		}

		this.recordingState = "Aufnahme läuft …";

		// Trigger auto stop if configured
		if (typeof this._element.duration === 'number' && this._element.duration > 0) {
			setTimeout(() => {
				this.stopRecording();
			}, this._element.duration);
		}

		this.recorder = new AudioRecorder();

		// Configure recorder according to element configuration
		if (this._element.sampleRate) {
			this.recorder.sampleRate = this._element.sampleRate;
		}

		if (this._element.bufferSize) {
			this.recorder.bufferSize = this._element.bufferSize;
		}

		if (typeof this._element.silenceDetection === 'number' && this._element.silenceDetection > 0) {
			this.recorder.silenceDetectionDuration = this._element.silenceDetection;
		} else {
			this.recorder.silenceDetectionDuration = 0;
		}

		// Kick off recorder
		this.recorder.record()
			.then((signal) => {
				this.recorder = null;
				this.recordingState = '';
				this.recordings.push(signal);
				this.result.emit(signal);
				if (!this._element.proceedButton) {
					this.finish.emit(undefined);
				}
			})
			.catch((error) => {
				console.log(error);
				this.recordingState = 'Fehler bei der Aufnahme.';
				this.recorder = null;
			});
	};
}
