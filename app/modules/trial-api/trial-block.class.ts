// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {KeyValuePair} from "../key-value-pair.class";
import {TrialElementDefinition} from "./trial-element-definition.interface";
import {CSVResultService} from "../result-services/csv-result.service";
import {EmuDBResultService} from "../result-services/emudb-result.service";
import {BundleResultDescription} from "./bundle-result-description.interface";

/**
 * A trial block is a series of trials that is presented to a subject. The
 * individual trials may depend on each other and/or only make sense in
 * connection with each other. They are therefore a unit and can never be
 * split up due to randomisation or anything else.
 *
 * Using an experiment definition, the experimenter provides a list of
 * stimulus objects for the trial block. The trial block is executed once (or
 * an experimeter-defined number of times) for each stimulus object.
 *
 * The unit is controlled by a class extending the TrialBlock class. The
 * derived class can be as simple as providing a member array named trials.
 * In that case – which is called the vanilla use of TrialBlock -, the trial
 * elements are matched to members of the stimulus objects based on their
 * names. The derived class may also override one or more of the API methods
 * init(stimulusObject), getNextTrial(), updateResult(preliminaryResult), and
 * finishTrial(), in order to attain some complex behaviour connecting the
 * individual trials. Overriding methods is called scripted use of TrialBlock.
 *
 * The results are saved by means of the protected methods saveCSVResults()
 * and saveEmuDBResults(), which are called by the public method finishTrial()
 * when the last trial is finished.
 *
 * saveCSVResults() will look for trial elements referenced by name in
 * csvColumns and pass their results to CSVResultService. Derived classes can
 * simply override the csvColumns property (which is an empty array by default),
 * or override the method.
 *
 * saveEmuDBResults() will look for trial elements referenced by name in
 * emuDBBundles[i].signalElement or emuDBBundles[i].bundleAnnotation and
 * pass them as signal or metadata, respectively, to EmuDBResultService.
 * Since emuDBBundles is an array, multiple bundles may be specified. Again,
 * derived classes can simply override the emuDBBundles property, or
 * override the method. Overriding the method is necessary to exploit most
 * features of the emuDB format, such as saving a segmentation of a signal.
 *
 * @author Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
 */
export class TrialBlock {
	/**
	 * The constructor accepts references to the result services. They would
	 * normally be provided by Angular's dependency injection, but that is
	 * not available since this class is not a component.
	 *
	 * The constructor usually does not need overriding. If you do anyway,
	 * remember to call this constructor using super().
	 *
	 * @param _csvResultService
	 * @param _emuDBResultService
	 */
	constructor(_csvResultService:CSVResultService, _emuDBResultService:EmuDBResultService) {
		this._csvResultService = _csvResultService;
		this._emuDBResultService = _emuDBResultService;
	}

	/**
	 * This function is called before the trial block is presented to a
	 * subject.
	 *
	 * The stimuli passed in are copied, such that modifying them at a later
	 * point outside of this class is not possible.
	 *
	 * Derived classes may exhibit asynchronous behaviour during init().
	 * This function therefore returns a promise that resolves to an
	 * undefined value as soon as the trial block has been initialised
	 * completely. It may be rejected if initialisation fails, e.g. if a
	 * necessary web service is not available. The vanilla implementation
	 * returns a promise that is immediately resolved.
	 *
	 * @param stimulusObject A list of named stimuli that are presented in
	 *                        the course of the trial block.
	 * @returns A promise that is resolved as soon as initialisation has
	 *           been completed. The promise is rejected if initialisation
	 *           fails.
	 * @override Unless vanilla implementation does the job you need.
	 */
	public init(stimulusObject:KeyValuePair<any>[]):Promise<void> {
		this.stimulusObject = KeyValuePair.copyList(stimulusObject);
		return Promise.resolve();
	}

	/**
	 * This function is called whenever the GUI needs to display a new
	 * trial. It returns a promise that resolves to that trial.
	 *
	 * A trial is an array of trial elements. This function uses a static,
	 * pre-defined trial, but dynamically modifies it before returning it.
	 *
	 * The basis is this.trials[this.currentTrialNumber].
	 * The `value` of each trial element can be modified in one of two ways:
	 *
	 * 1) If the trial element has a property `usePreviousResult`, then a named
	 *    result is searched whose name is the value of said property. The
	 *    named result is used as the trial element's value. The named
	 *    result is searched among the previous trials within this trial block.
	 *
	 * 2) If a named stimulus is found whose name is the same as the trial
	 *    element's name, then that stimulus is used as the value. The named
	 *    stimuli are searched within this.stimulusObject, which is passed
	 *    in via init() - after it has been defined by the experimenter.
	 *
	 * Number 1 takes precedence over number 2 if both are possible.
	 *
	 * The returned trial must be regarded as a reference, i.e. whoever
	 * displays it must react when this class makes changes to it at a later
	 * point. However, the trial should not be modified outside of this class.
	 *
	 * Derived classes may rely on some asynchronous behaviour to generate
	 * the next trial. This function therefore does not return a direct
	 * reference to a trial, but rather a promise that resolves to the
	 * desired next trial. It resolves to null if the trial block is over
	 * (i.e. there are no trials left). It may be rejected if the next trial
	 * cannot be generated (e.g. because a necessary web service is not
	 * available. The vanilla implementation returns a promise that is
	 * immediately resolved.
	 *
	 * @returns A promise resolving to a reference to a trial (i.e., an
	 *           array of trial elements) or null if the trial block is over.
	 *
	 * @override Unless vanilla implementation does the job you need.
	 */
	public getNextTrial():Promise<TrialElementDefinition<any>[]> {
		this.currentTrialNumber++;

		if (this.currentTrialNumber >= this.trials.length) {
			return Promise.resolve(null);
		}

		var nextTrial:TrialElementDefinition<any>[] = this.trials[this.currentTrialNumber];

		// Incorporate named stimuli and named results into trial
		for (var i = 0; i < nextTrial.length; ++i) {
			if (nextTrial[i].usePreviousResult !== undefined) {
				var value:any = this.findNamedResult(nextTrial[i].usePreviousResult);
				if (value !== undefined) {
					nextTrial[i].value = value;
				}
			} else {
				var value:any = this.findNamedStimulus(nextTrial[i].name);
				if (value !== undefined) {
					nextTrial[i].value = value;
				}
			}
		}

		return Promise.resolve(nextTrial);
	}

	/**
	 * This function is called whenever the subject has used a trial
	 * element. Pass in the new state (=preliminary result) of the trial.
	 *
	 * {result} is stored as a reference. It can therefore still be modified
	 * by its creator (or whoever has gotten hold of a reference). A copy is
	 * only made by finishTrial().
	 *
	 * @param {KeyValuePair<any>[]} result The new state of the trial
	 * @override Unless vanilla implementation does the job you need.
	 */
	public updateResult(preliminaryResult:KeyValuePair<any>[]):void {
		this.state = preliminaryResult;
	}

	/**
	 * This function is called whenever the subject has performed an action
	 * that finishes the current trial.
	 *
	 * Makes a copy of the preliminary result passed in via updateResult().
	 * It can then no longer be modified outside of this class.
	 *
	 * When the last trial has been finished, the copied result of this and
	 * all previous trials are fed into the result services.
	 *
	 * @override Unless vanilla implementation does the job you need.
	 */
	public finishTrial():void {
		this.results.push(KeyValuePair.copyList(this.state));
		this.state = [];

		if (this.currentTrialNumber + 1 === this.trials.length) {
			this.saveCSVResults();
			this.saveEmuDBResults();
		}
	}

	/**
	 * This function is called by the vanilla implementation of
	 * finishTrial() when the last trial has been finishedd.
	 *
	 * @override Unless vanilla implementation does the job you need.
	 */
	protected saveCSVResults():void {
		var observation:KeyValuePair<any>[] = [];

		// Collect all named results referenced in this.csvColumns.
		// Make sure all names referenced in this.csvColumns exist as
		// keys in observation. If possible, find a corresponding value in the
		// results. If no such value exists, leave the value undefined.
		for (var i = 0; i < this.csvColumns.length; ++i) {
			observation.push({
				key: this.csvColumns[i],
				value: this.findNamedResult(this.csvColumns[i])
			});
		}

		// Submit result
		this._csvResultService.addObservation(observation);
	}

	/**
	 * This function is called by the vanilla implementation of
	 * finishTrial() when the last trial has been finished.
	 *
	 * When overriding this function, you may call this version via
	 * super.saveEmuDBResults(false). The false effects that the results are
	 * prepared and returned, but not actually submited to the result service.
	 *
	 * @param {boolean} saveResults Submit the result to the result service
	 *                              or just prepare and return it?
	 * @returns {{name: string, annotation: KeyValuePair<any>[], signal: AudioBuffer}}
	 *           The results that can be submitted to the result service.
	 *
	 * @override Unless vanilla implementation does the job you need.
	 */
	protected saveEmuDBResults(saveResults:boolean=true):Array<{
		name:string,
		annotation:KeyValuePair<any>[],
		signal:AudioBuffer
	}> {
		// Process this.emuDBBundles: It is an array whose elements specify
		// one bundle each.
		//
		// All bundles produced by one trial block should
		// be grouped together, we therefore request a name prefix from
		// EmuDBResultService.
		//
		var namePrefix:string = this._emuDBResultService.getNewBundleNamePrefix();

		var results = [];

		for (var i = 0; i < this.emuDBBundles.length; ++i) {
			var name:string = namePrefix + "_" + this.emuDBBundles[i].bundleName;
			var annotation:KeyValuePair<any>[] = [];
			var signal:AudioBuffer = this.findNamedResult(this.emuDBBundles[i].signalElement);

			// Search values for annotation
			for (var j = 0; j < this.emuDBBundles[i].bundleAnnotation.length; ++j) {
				annotation.push({
					key: this.emuDBBundles[i].bundleAnnotation[j],
					value: this.findNamedResult(this.emuDBBundles[i].bundleAnnotation[j])
				});
			}

			// Submit result
			if (saveResults) {
				this._emuDBResultService.addSimpleBundle(name, annotation, signal);
			}

			results.push({
				name: name,
				annotation: annotation,
				signal: signal
			});
		}

		return results;
	}

	/**
	 * Search a named result in all trials that have been finished so far.
	 *
	 * A result of undefined can mean that the name is not available or that
	 * an undefined result was yielded by the according trial element.
	 *
	 * @param {string} name The name of a trial element that is part of {trials}
	 * @returns {any} The result yielded by the trial element identified by
	 * {name}.
	 */
	protected findNamedResult(name:string):any {
		for (var i = 0; i < this.results.length; ++i) {
			var pair:KeyValuePair<any> = KeyValuePair.findKey(this.results[i], name);
			if (pair !== undefined) {
				return pair.value;
			}
		}
		return undefined;
	}

	/**
	 * Search a named stimulus in this.stimulusObject (passed in via init()).
	 *
	 * A result of undefined can mean that the name is not available or that
	 * the value associated with the name is undefined.
	 *
	 * @param {string} name A name of a stimulus that is part of
	 *                       {stimulusObject} (which is passed in via init()).
	 * @returns {any} The value associated with the stimulus identified by
	 *                 {name}.
	 */
	protected findNamedStimulus(name:string):any {
		var pair:KeyValuePair<any> = KeyValuePair.findKey(this.stimulusObject, name);
		if (pair !== undefined) {
			return pair.value;
		} else {
			return undefined;
		}
	}

	//////////
	// Private/protected members
	//

	// These are only to keep track of what's going on in the current
	// instance of TrialBlock. Remember that this class can be used in a
	// "vanilla way" (not overriding any of its methods, but only its
	// member fields). These variables here are needed by the vanilla
	// implementation and should not be overridden unless the methods are
	// also overridden.

	// Holds a copy of the stimulusObject passed to init()
	protected stimulusObject:KeyValuePair<any>[] = [];
	// Holds the index of the trial (within the array trials) the subject is
	// currently at. Is incremented by getNextTrial().
	protected currentTrialNumber:number = -1;
	// Holds a reference to the preliminary result of the current trial. This is
	// passed in via updateResult().
	protected state:KeyValuePair<any>[] = [];
	// Holds a copy of the result of all trials. This is snapshotted from
	// {state} when finishTrial() is called.
	protected results:KeyValuePair<any>[][] = [];

	//////////
	// Define the trials
	//

	/**
	 * An ordered list of trials that form the basis of this trial block. A
	 * trial is defined as an array of TrialElementDefinition. The type of
	 * trials is therefore an array of arrays of TrialElementDefinition.
	 *
	 * Every trial element has at least a type, usually also a name and a
	 * value. See documentation (which does not yet exist, what a pity) to
	 * find out what types are allowed. The value is usually a stimulus or
	 * an informative text that will be presented to the subject.
	 *
	 * The name of the trial elements is most important because it can be
	 * referenced in {csvColumns} and {emuDBBundles} to specify which trial
	 * elements yield results that will be saved. It is also referenced in
	 * init()'s stimulusObject parameter to specify which stimuli are
	 * presented by which trial elements.
	 *
	 * @type {TrialElementDefinition[][]}
	 * @override
	 */
	protected trials:TrialElementDefinition<any>[][] = [];

	//////////
	// Saving results
	//

	/**
	 * The variables csvColumns and emuDBBundles are the way to specify, which
	 * of the trial block's trial elements produce data thate are saved with
	 * the results.
	 *
	 * csvColumns is an array that specifies names of trial elements (not
	 * their types). The result of all trial elements identified this way
	 * are fed into CSVResultService.
	 *
	 * @type {String[]}
	 * @override
	 */
	protected csvColumns:string[] = [];

	/**
	 * The variables csvColumns and emuDBBundles are the way to specify, which
	 * of the trial block's trial elements produce data thate are saved with
	 * the results.
	 *
	 * emuDBBundles is an array that specifies a list of simple bundles.
	 * These are fed into EmuDBResultService.addSimpleBundle().
	 *
	 * Each bundle description is composed of three elements:
	 *
	 * * bundleName specifies the name of the bundle itself (the helper
	 *   function EmuDBResultService.getNewBundleNamePrefix() helps you
	 *   construct one)
	 * * signalElement specifies the name (not the type) of a trial element
	 *   that yields an audio signal as its result.
	 * * bundleAnnotation is an array that specifies names of trial elements
	 *   (not their types). The result of all trial elements identified this
	 *   way are fed into EmuDBResultService.
	 *
	 * @type {BundleResultDescription{}}
	 * @override
	 */
	protected emuDBBundles:BundleResultDescription[] = [];

	// These are the services that we feed our results into.
	//
	// A note on Angular: The services are assumed to be Angular services,
	// which are singleton references, but they might be anything implementing
	// the correct API. Usually they would be accessed via Angular's
	// dependency injection system, but that is not available since this
	// class is not a component.
	//
	protected _csvResultService:CSVResultService;
	protected _emuDBResultService:EmuDBResultService;
}
