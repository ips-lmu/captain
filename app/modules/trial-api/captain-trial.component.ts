// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component} from 'angular2/core';
import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

import {TrialElementDefinition} from "./trial-element-definition.interface";
import {KeyValuePair} from "../key-value-pair.class";

import {CaptainAudioRecorder} from "./trial-elements/captain-audio-recorder.component";
import {CaptainCaption} from "./trial-elements/captain-caption.component";
import {CaptainAudioPlayer} from "./trial-elements/captain-audio-player.component";
import {CaptainSlider} from "./trial-elements/captain-slider.component";
import {CaptainSubmit} from "./trial-elements/captain-submit.component";
import {CaptainButtonList} from "./trial-elements/captain-button-list.component";


@Component({
	selector: 'captain-trial',
	directives: [CaptainAudioRecorder, CaptainCaption, CaptainAudioPlayer,
		CaptainSlider, CaptainSubmit, CaptainButtonList],
	template: `
		<template ngFor #element [ngForOf]="elements">
			<div [ngSwitch]="element.type">
				<captain-caption
						*ngSwitchWhen="'caption'"
						[element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-caption>
				<captain-audio-player
						*ngSwitchWhen="'audioPlayer'"
						[element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-audio-player>
				<captain-slider
						*ngSwitchWhen="'slider'"
						[element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-slider>
				<captain-submit
						*ngSwitchWhen="'submit'"
						[element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-submit>
				<captain-button-list
						*ngSwitchWhen="'buttonList'"
						[element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-button-list>
				<captain-audio-recorder
						*ngSwitchWhen="'audioRecorder'"
				        [element]="element"
						(result)="changeResult(element.name, $event)"
						(finish)="finishTrial()"></captain-audio-recorder>
			</div>
		</template>
	`
})
export class CaptainTrial {
	@Input() elements:TrialElementDefinition<any>[];
	@Output() result:EventEmitter<KeyValuePair<any>[]> = new EventEmitter<KeyValuePair<any>[]>();
	@Output() finish:EventEmitter<void> = new EventEmitter<void>();

	private resultSet:KeyValuePair<any>[] = [];

	private changeResult(elementName:string, value:any):void {
		var pair:KeyValuePair<any> =
			KeyValuePair.findKey(this.resultSet, elementName);

		if (pair === undefined) {
			this.resultSet.push({
				key: elementName,
				value: value
			});
		} else {
			pair.value = value;
		}

		this.result.emit(this.resultSet);
	}

	private finishTrial() {
		this.finish.emit(null);

		this.resultSet = [];
		this.result.emit(this.resultSet);
	}
}
