// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {BundleResultDescription} from "../trial-api/bundle-result-description.interface";
import {KeyValuePair} from "../key-value-pair.class";

export class ForcedChoiceTrialBlock extends TrialBlock {
	protected csvColumns:string[] = ['prompt', 'choice', 'confidence', 'nonexistentProperty'];
	protected emuDBBundles:BundleResultDescription[] = [{
		bundleName: 'forcedchoice',
		signalElement: '',
		bundleAnnotation: ['prompt', 'choice', 'confidence']
	}];

	protected trials:TrialElementDefinition<any>[][] = [
		// First trial
		[{
			type: 'caption',
			name: 'prompt'
		}, {
			type: 'buttonList',
			name: 'choice'
		}],

		// Second trial
		[{
			type: 'caption',
			value: 'Ihre Antwort war:',
		}, {
			type: 'caption',
			usePreviousResult: 'choice'
		}, {
			type: 'caption',
			value: 'Wie sicher waren Sie sich bei Ihrer Antwort?'
		}, {
			type: 'slider',
			name: 'confidence'
		}, {
			type: 'submit',
			value: 'Weiter',
		}]
	];

	public updateResult(preliminaryResult:KeyValuePair<any>[]):void {
		super.updateResult(preliminaryResult);

		var pair:KeyValuePair<any> =
			KeyValuePair.findKey(preliminaryResult, 'confidence');

		if (pair !== undefined && pair.value > 80) {
			this.trials[this.currentTrialNumber][2].value = ['Wirklich so' +
			' sicher?'];
		}
	}
}
