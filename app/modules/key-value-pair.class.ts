// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * A generic class to hold a key-value pair. Also offers some static
 * functions to deal with list of key-value pairs.
 */
export class KeyValuePair<T> {
	public key:string;
	public value:T;

	/**
	 * Create a deep copy of an list of key-value pairs
	 *
	 * @param {KeyValuePair[]} source A reference to a list of key-value pairs
	 * @returns {KeyValuePair[]} A copy of {source}
	 */
	public static copyList (source:KeyValuePair<any>[]):KeyValuePair<any>[] {
		var copy:KeyValuePair<any>[] = [];

		for (var i=0; i<source.length; ++i) {
			copy.push({
				key: source[i].key,
				// @todo IMPORTANT do not create shallow copy
				value: source[i].value
			})
		}

		return copy;
	}

	/**
	 * Search a list of key-value pairs for a certain key.
	 *
	 * @param {KeyValuePair<any>[]} list The list that {key} is searched in.
	 * @param {string} key The key that is searched in {list}.
	 * @returns {KeyValuePair<any>} The respective pair or undefined if {key}
	 *                          does not exist in {list}.
	 */
	public static findKey (list:KeyValuePair<any>[], key:string):KeyValuePair<any> {
		for (var i=0; i < list.length; ++i) {
			if (list[i].key === key) {
				return list[i];
			}
		}
		return undefined;
	}
}
