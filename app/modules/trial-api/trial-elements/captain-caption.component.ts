// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component, Pipe, PipeTransform} from "angular2/core";
import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";
import {OnInit} from "angular2/core";

import {CaptainTrialElement} from "../captain-trial-element.class";
import {TrialElementDefinition} from "../trial-element-definition.interface";

@Pipe({name: 'newLine'})
class NewLinePipe implements PipeTransform {
	transform(value:string):string {
		if (typeof value === 'string') {
			return value.replace(/\n/g, "<br>");
		} else {
			return value;
		}
	}
}

@Component({
	selector: 'captain-caption',
	pipes: [NewLinePipe],
	template: `
		<div [innerHTML]="element.value | newLine"></div>
		<p class="component-label">caption</p>
	`
})
export class CaptainCaption extends CaptainTrialElement implements OnInit {
	@Input() element:TrialElementDefinition<string>;
	@Output() result:EventEmitter<string> = new EventEmitter<string>();

	ngOnInit() {
		this.result.emit(this.element.value);
	}
}
