import {Component, HostBinding} from "angular2/core";
import {OnInit} from "angular2/core";
import {Router} from "angular2/router";
import {RouteParams} from "angular2/router";
import {ROUTER_DIRECTIVES} from "angular2/router";

import {CaptainExperiment} from "./modules/experiment-api/captain-experiment.component";
import {ExperimentStore} from "./modules/experiment-store/experiment-store.service";
import {ExperimentDefinition} from "./modules/experiment-api/experiment-definition.interface";


@Component({
	selector: 'captain-laboratory',
	directives: [CaptainExperiment, ROUTER_DIRECTIVES],
	template: `
		<ul *ngIf="!experimentRunning">
			<li *ngFor="#experiment of experiments">
				<a [routerLink]="['Experiment', {experimentName: experiment.name}]">
					{{experiment.name}}
				</a>
			</li>
		</ul>
		<div *ngIf="experimentFinished">
			The experiment has been finished.
		</div>
		<div *ngIf="experimentFinished && errors.length" class="error">
			<p>Errors during the experiment:</p>
			<ul>
				<li *ngFor="#error of errors">{{error}}</li>
			</ul>
			<p>See browser console for details.</p>
		</div>
		<captain-experiment *ngIf="experimentRunning"
							[data]="experimentData"
							(end)="endExperiment()"
							(error)="experimentError($event)">
		</captain-experiment>
	`,
	styles: [`
		:host(.experimentRunning) {
			background-color: white;
			position: absolute;
		    width: 100%;
		    height: 100%;
		    top: 0;
		    left: 0;
		}
		
		.error {
			background-color: lightcoral;
			border: 1px dashed black;
			padding: 10px;
		}
	`]
})
export class CaptainLaboratory implements OnInit {
	constructor(private _router:Router,
	            private _routeParams:RouteParams,
	            private _experimentStore:ExperimentStore) {
	}

	ngOnInit() {
		var experiment = this._routeParams.get('experimentName');

		if (experiment === null) {
			this.experiments = this._experimentStore.getExperimentList();
			this.experimentRunning = false;
		} else {
			this.experimentData = this._experimentStore.getExperimentByName(experiment);
			this.experimentRunning = true;
		}
	}
	
	private experimentError(error) {
		this.errors.push(error.message);
		console.log(error);
	}

	private endExperiment():void {
		this.experimentRunning = false;
		this.experimentFinished = true;
	}

	private experimentData:ExperimentDefinition;
	private experiments:{name:string, uuid:string}[] = [];

	@HostBinding('class.experimentRunning') private experimentRunning:boolean = false;
	private experimentFinished:boolean = false;
	private errors:string[] = [];
}
