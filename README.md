# Captain

Captain is a research tool designed for the scientific evaluation of methods in
computer-assisted pronunciation training (CAPT). The idea was formed at a workshop
organised by Saarland University (see [IFCASL](http://www.ifcasl.org/feedback_workshop.html)).

The tool is a work-in-progress, but has successfully been used in one pilot study
(Jochim & Draxler, 2016) to evaluate an automatic accent correction method.

The major goals for Captain are

1. to re-use participant responses as stimuli in later trials, without manual intervention and
2. to employ complex signal processing methods at the time of conducting the experiment, without manual intervention.

## Online Demo

An online version of Captain is available at https://www.phonetik.uni-muenchen.de/apps/captain/.

## Notes for Developers

@todo Notes for developers

## References

Jochim, M. & Draxler, C. (2016). Fully Automated Accent Correction for Computer-Assisted Speech Rhythm Training. In *Proceedings P&P12*.