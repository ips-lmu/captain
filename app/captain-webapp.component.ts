import {Component} from 'angular2/core';
import {OnInit} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router'

import {CaptainInfo} from "./captain-info";
import {CaptainLaboratory} from "./captain-laboratory.component";
import {CaptainWorkshop} from "./captain-workshop.component";

import {TrialBlockStore} from "./modules/trial-block-store/trial-block-store.service";
import {ExperimentStore} from "./modules/experiment-store/experiment-store.service";
import {CSVResultService} from "./modules/result-services/csv-result.service";
import {EmuDBResultService} from "./modules/result-services/emudb-result.service";

@Component({
	selector: 'captain',
	template: `
		<div>
			<h1><a [routerLink]="['Info']">Captain</a></h1>
			<ul>
				<li><a [routerLink]="['Laboratory']">Labor</a></li>
				<li><a [routerLink]="['Workshop', {roomName: 'entryHall'}]">								Werkstatt</a></li>
				<li><a>Lernen?</a></li>
			</ul>
		</div>
		<router-outlet></router-outlet>
	`,
	directives: [ROUTER_DIRECTIVES],
	providers: [CSVResultService, EmuDBResultService, TrialBlockStore, ExperimentStore, ROUTER_PROVIDERS],
	styles: [`
		:host {
			display: flex;
			flex-direction: row;
		}

		:host router-outlet + * {
			flex-basis: 100%;
			padding: 10px;
		}

		:host > div {
			background-color: green;
			text-align: center;
		}

		:host > div > h1 {
			padding: 0px 20px;
		}
		
		:host > div > h1 > a {
			text-decoration: none;
			color: black;
		}

		:host > div > ul {
			list-style-type: none;
			padding: 0;
		}

		:host > div > ul a  {
			line-height: 50px;
			height: 50px;
			color: white;
			display: block;
			text-decoration: none;
			font-family: sans-serif;
			font-variant: small-caps;

		}

		:host > div > ul a:hover {
			background-color: #006000;
		}
		
		:host > captain-workshop {
			overflow: auto;
		}
	`]
})
@RouteConfig([
	{path: '/laboratory/:experimentName', name: 'Experiment', component: CaptainLaboratory},
	{path: '/laboratory', name: 'Laboratory', component: CaptainLaboratory},
	{path: '/workshop/:roomName', name: 'Workshop', component: CaptainWorkshop},
	{path: '/', name: 'Info', component: CaptainInfo},
])
export class CaptainWebapp implements OnInit {
	constructor() {
	}

	ngOnInit() {
	}
}
