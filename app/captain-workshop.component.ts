import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {NgClass} from "angular2/common";
import {NgModel} from "angular2/common";

import {Router} from "angular2/router";
import {RouteParams} from "angular2/router";
import {ROUTER_DIRECTIVES} from "angular2/router";

import {ExperimentDefinition} from "./modules/experiment-api/experiment-definition.interface";
import {ExperimentSection} from "./modules/experiment-api/experiment-definition.interface";
import {ExperimentStore} from "./modules/experiment-store/experiment-store.service";

import {TrialBlockStore} from "./modules/trial-block-store/trial-block-store.service";

import {CaptainTrial} from "./modules/trial-api/captain-trial.component";
import {CaptainAudioRecorder} from "./modules/trial-api/trial-elements/captain-audio-recorder.component";
import {CaptainSubmit} from "./modules/trial-api/trial-elements/captain-submit.component";
import {CaptainAudioPlayer} from "./modules/trial-api/trial-elements/captain-audio-player.component";
import {CaptainButtonList} from "./modules/trial-api/trial-elements/captain-button-list.component";
import {CaptainCaption} from "./modules/trial-api/trial-elements/captain-caption.component";
import {CaptainSlider} from "./modules/trial-api/trial-elements/captain-slider.component";





@Component({
	selector: 'captain-workshop',
	templateUrl: 'app/captain-workshop.template.html',
	styles: [`
		:host {
			display: flex;
			flex-direction: column;
		}

		:host div {
		}

		.help-screen {
			border-top: 5px double black;
			flex-grow: 0;
			transition: all 300ms linear;
			order: 2;
			overflow: hidden;
			min-height: 0px;
			height: 50px;
		}

		.help-screen.open {
			flex-grow: 1;
			min-height: 200px;
			overflow: auto;
		}

		.help-screen .close {
			float: right;
			border: 1px solid black;
			border-radius: 10px;
			background-color: red;
		}

		.main-content {
			flex-basis: 100%;
			flex-grow: 1;
		}

		.elementContainer {
			background-color: #CCC;
			text-align: center;
			padding: 10px;
			margin: 10px;
		}

		.dragTarget:before {
			display: block;
			content: '';
			position: relative;
			height: 50px;
			border: 1px dashed black;
		}

		:host .component-label {
			text-align: right;
			font-family: monospace;
			color: #999;
		}

		.menu {
			padding: 0;
			margin: 0;
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			align-content: stretch;
		}

		.menu > li {
			flex-grow: 1;
			min-height: 70px;
			display: flex;
		}

		.menu > li a {
			flex-basis: 100%;
			margin: 5px;

			border: 1px solid black;
			border-radius: 5px;

			display: flex;
			align-items: center;
			justify-content: center;

			text-decoration: none;
			color: inherit;
		}

		.menu > li a:hover {
			background-color: grey;
		}

		.block-separator {
			text-align: center;
			font-weight: bold;
			display: flex;
			align-items: center;
		}

		.block-separator span {
			margin: 0 10px;
		}

		.block-separator:before,
		.block-separator:after {
			flex-grow: 1;
			content: '';
			background-color: black;
			height: 2px;
		}
			
		:host captain-trial > * {
			background-color: #CCC;
			text-align: center;
			padding: 10px;
			margin: 10px;
		}

		:host captain-trial .component-label {
			text-align: right;
			font-family: monospace;
			color: #999;
		}
	`],
	directives: [NgClass, NgModel, ROUTER_DIRECTIVES, CaptainAudioRecorder, CaptainSubmit, CaptainAudioPlayer,
		CaptainButtonList, CaptainCaption, CaptainSlider
	]
})
export class CaptainWorkshop implements OnInit {
	constructor (
		private _router: Router,
		private _routeParams: RouteParams,
		private _experimentStore: ExperimentStore,
	    private _trialBlockStore: TrialBlockStore
	) {}

	elementList:any[] = [];

	availableElements:any[] = [
		{type: 'audioRecorder'},
		{type: 'submit', value: 'Fertig'},
		{type: 'audioPlayer'},
		{type: 'buttonList', value: ['Mehrere', 'Forced-Choice', 'Buttons']},
		{type: 'caption', value: 'Das hier ist eine Freitext-Komponente'},
		{type: 'slider'}
	];

	addElement(element):void {
		this.elementList.push(element);
	}

	dragstart(element):void {
		this.elementDragged = element;
	}

	dragover(event):void {
		event.dataTransfer.dropEffect = 'move';
		event.preventDefault();
	}

	dragenter(element):void {
		this.dragTarget = element;
	}

	drop(element):void {
		if (element !== this.elementDragged) {
			var dragIndex = this.elementList.indexOf(this.elementDragged);
			this.elementList.splice(dragIndex, 1);

			var dropIndex = this.elementList.indexOf(element);
			this.elementList.splice(dropIndex, 0, this.elementDragged);
		}
		this.dragTarget = null;
		this.elementDragged = null;
	}

	ngOnInit() {
		this.room = this._routeParams.get('roomName');
		this.experimentList = this._experimentStore.getExperimentList();
	}

	private editExperiment(uuid:string) {
		this.currentExperiment = this._experimentStore.getExperimentByUUID(uuid);
		this.currentExperimentJSON = JSON.stringify(this.currentExperiment, null, 2);
	}

	private newExperiment() {
		this.currentExperiment = this._experimentStore.addExperiment();
		this.currentExperimentJSON = JSON.stringify(this.currentExperiment, null, 2);
	}

	private saveChanges() {
		if (this.currentExperimentJSON !== '') {
			for (var i in this.currentExperiment) {
				delete this.currentExperiment[i];
			}
			Object.assign(this.currentExperiment, JSON.parse(this.currentExperimentJSON));
		}
		this._experimentStore.saveToBrowser();
	}

	private addSection(experiment:ExperimentDefinition) {
		experiment.sections.push({
			randomise: false,
			trialBlocks: []
		})
	}

	private addTrialBlock(section:ExperimentSection) {
		section.trialBlocks.push({
			type: '',
			repetitions: 1,
			stimulusObjects: []
		});
	}

	private dragTarget;
	private elementDragged;

	private experimentList;
	private currentExperiment:ExperimentDefinition;
	private currentExperimentJSON:string = '';

	private room:string;
	private helpScreen = {
		text: '',
		textA: 'Gallia est omnis divisa in partes tres, quorum unum incolunt' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' a nadie. Me pueden una vez. Me pueden tener caro. Estoy fuera.' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo' +
		' Beligae, aliud tribus qui sua lingua Celtae, nostra Germanii' +
		' appeluntur. Interessante est quod te digo, por que se llama' +
		' Lamajor y es un bot, Boten Ana. Ana heter hon. Nunca he creido que' +
		' se haya a ver con estos estupendos pollos rellenos de polla. Pero' +
		' a mi no se cree por que estoy un pobre hombre sin dinero que ya no' +
		' sabe nada y nunca ha encontrado a nadie. Nadir es muy divertido' +
		' pero eso no puedo tampoco. La gente que vive aqui siempre me ha' +
		' dicho que las geisladiskurnir son muchíssimos. Pero yo ne le creo'
	}
}
