// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Injectable} from "angular2/core";
import {TrialBlock} from "../trial-api/trial-block.class";
import {CSVResultService} from "../result-services/csv-result.service";
import {EmuDBResultService} from "../result-services/emudb-result.service";

import {ForcedChoiceTrialBlock} from "./forced-choice-trial-block.class";
import {PsolaFeedbackTrialBlock} from "./psola-feedback-trial-block.class";
import {DescriptionTrialBlock} from "./description-trial-block.class";
import {NullFeedbackTrialBlock} from "./null-feedback-trial-block.class";
import {TestRecorderTrialBlock} from "./test-recorder-trial-block";

@Injectable()
export class TrialBlockStore {
	constructor(private _csvResultService:CSVResultService,
	            private _emuDBResultService:EmuDBResultService) {
	}

	public getTrialBlockInstance(type:string):TrialBlock {
		console.log('Factoring ', type);
		if (type === 'ForcedChoice') {
			return new ForcedChoiceTrialBlock(this._csvResultService, this._emuDBResultService);
		} else if (type === 'PsolaFeedback') {
			return new PsolaFeedbackTrialBlock(this._csvResultService, this._emuDBResultService);
		} else if (type === 'Description') {
			return new DescriptionTrialBlock(this._csvResultService, this._emuDBResultService);
		} else if (type === 'NullFeedback') {
			return new NullFeedbackTrialBlock(this._csvResultService, this._emuDBResultService);
		} else if (type === 'TestRecorder') {
			return new TestRecorderTrialBlock(this._csvResultService, this._emuDBResultService);
		} else {
			return new TrialBlock(this._csvResultService, this._emuDBResultService);
		}
	}

	public availableTrialBlockTypes():string[] {
		return ['Description', 'ForcedChoice', 'PsolaFeedback', 'NullFeedback', 'TestRecorder'];
	}
}
