// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {KeyValuePair} from "../key-value-pair.class";

export class DescriptionTrialBlock extends TrialBlock {
	public init(stimulusObject:KeyValuePair<any>[]):Promise<void> {
		super.init(stimulusObject);

		if (KeyValuePair.findKey(this.stimulusObject, 'button') === undefined) {
			this.trials[0].pop();
		}

		return Promise.resolve();
	}

	protected trials:TrialElementDefinition<any>[][] = [
		[{
			type: 'caption',
			name: 'text'
		}, {
			type: 'submit',
			name: 'button'
		}],
	];
}
