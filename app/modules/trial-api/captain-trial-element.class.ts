// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

import {TrialElementDefinition} from "./trial-element-definition.interface";

export class CaptainTrialElement {
	@Input() element:TrialElementDefinition<any>;
	@Output() result:EventEmitter<any> = new EventEmitter<any>();
	@Output() finish:EventEmitter<void> = new EventEmitter<void>();
}
