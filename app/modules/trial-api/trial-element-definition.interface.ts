// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

export interface TrialElementDefinition<T> {
	type: string;
	name?: string;
	value?: T;
	usePreviousResult?: string;
}
