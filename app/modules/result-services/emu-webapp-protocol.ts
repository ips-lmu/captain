// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
// (c) Raphael Winkelmann <raphael@phonetik.uni-muenchen.de>
// (c) Georg Raess <graess@phonetik.uni-muenchen.de>

import {generateUUID} from "../generate-uuid.function";
import {EmuDBBundleAnnotation} from "./emudb-bundle-annotation.interface";

export class WebSocketError extends Error {
	public name = 'WebSocketError';
}

export interface PendingRequest {
	timerID:number;
	resolve:Function;
	reject:Function;
}

export interface RequestCollection {
	[index:string]:PendingRequest;
}

export interface EmuDBBundleInTransit {
	annotation?: EmuDBBundleAnnotation;
	mediaFile?: {data: string, encoding: 'BASE64'};
	ssffFiles?: Array<any>;
	session?: string;
}

/**
 * This class represents one connection to a server speaking the EMU-webApp
 * protocol.
 *
 * The connection is started via init(). Afterwards, protocol-specific
 * functions can be called, such as getBundle(). All of these functions return
 * a promise that eventually resolves to the server's response. If the
 * server's response status is ERROR, or if the request times out, the
 * promise is rejected.
 *
 * As per the protocol specification, the functions need to be called in
 * this order:
 *
 * * getProtocol()
 * * getDoUserManagement()
 * * logOnUser() [only if getDoUserManagement() returned yes]
 * * getDbConfig()
 * * getBundleList() [if a bundle list is required]
 *
 * The remaining functions can be called in any order.
 *
 * This class fires error events when the connection is lost or when an
 * invalid packet is received. React to those events via addEventListener().
 *
 * Error events are different from rejected promises. The rejection reason
 * for promises is an object with the properties message (string) and data
 * (any).
 *
 */
export class EMUWebappProtocolConnection implements EventTarget {
	//////////
	// Configuration variables - can be changed via init()
	/** URL of the server to connect to */
	private serverURL:string;
	/** Interval to wait for replies from server before timing out (in ms) */
	private timeoutInterval:number = 30000;
	//
	//////////

	//////////
	// Status variables
	//
	/** WebSocket connection object */
	private ws:WebSocket;
	/** Keep all pending requests here until they get responses */
	private pendingRequests:RequestCollection = {};
	/** Pointer to a function that resolves the initial connection promise */
	private resolveConnectionPromise:(value?:MessageEvent | Thenable<MessageEvent>) => void;
	/** Pointer to a function that rejects the initial connection promise */
	private rejectConnectionPromise:(error?:any) => void;
	/** Whether the connection has been opened or not */
	private initialised:boolean = false;
	//
	//////////

	//////////
	// Implement EventTarget
	//
	/** Registered error event listeners */
	private eventListeners:EventListener[] = [];

	public addEventListener(type:string, listener:EventListener):void {
		if (type === 'error') {
			this.eventListeners.push(listener);
		}
	}

	public dispatchEvent(event:Event):boolean {
		for (let i = 0; i < this.eventListeners.length; ++i) {
			this.eventListeners[i](event);
		}
		return true;
	}

	public removeEventListener(type:string, listener:EventListener):void {
		for (let i = 0; i < this.eventListeners.length; ++i) {
			if (this.eventListeners[i] === listener) {
				this.eventListeners.splice(i, 1);
				return;
			}
		}
	}
	//
	//////////

	//////////
	// WebSocket functions
	//
	private wsonopen(message) {
		this.initialised = true;
		this.resolveConnectionPromise(message);
	}

	private wsonmessage(message) {
		this.handleResponse(JSON.parse(message.data));
	}

	private wsonerror(message) {
		var event = new ErrorEvent('error', {
			message: 'Unknown WebSocket error',
			error: message
		});
		this.dispatchEvent(event);
	}

	private wsonclose(message) {
		if (!this.initialised) {
			this.rejectConnectionPromise();
		} else {
			if (!message.wasClean) {
				var event = new ErrorEvent('error', {
					message: 'Non-clean disconnect from the server. This' +
					' probably means that the server is down. Please check the' +
					' server and reconnect!',
					error: message
				});
				this.dispatchEvent(event);
			}
		}
	}

	private sendRequest(request) {
		return new Promise<any>((resolve, reject) => {
			if (this.ws.readyState !== this.ws.OPEN) {
				throw new Error("WebSocket connection not open. Call init()" +
					" first.")
			}

			// Add a callback ID to request
			var callbackID = generateUUID();
			request.callbackID = callbackID;

			// Send request
			this.ws.send(JSON.stringify(request));

			// Fake a response after a certain timeout interval
			if (this.timeoutInterval !== 0) {
				var timerID = setTimeout(() => {
					this.handleResponse({
						callbackID: callbackID,
						data: this.timeoutInterval,
						status: {
							type: 'TIMEOUT',
							message: 'Request of type ' + request.type + ' timed out after ' + this.timeoutInterval + 'ms!  Please check the server...'
						}
					});
				}, this.timeoutInterval);
			}

			// Add request to list of pending
			this.pendingRequests[callbackID] = {
				timerID: timerID,
				resolve: resolve,
				reject: reject
			};
		});
	}

	private handleResponse(response) {
		// If we have a pending request with the response's callbackID, we
		// resolve it
		if (this.pendingRequests.hasOwnProperty(response.callbackID)) {
			// Remove the timer that would trigger a timeout
			clearTimeout(this.pendingRequests[response.callbackID].timerID);

			if (response.status.type === 'SUCCESS') {
				// Resolve promise with data only
				this.pendingRequests[response.callbackID].resolve(response);
			} else {
				// show protocol error and disconnect from server
				this.pendingRequests[response.callbackID].reject({
					message: response.status.message,
					data: response
				});
				this.close();
			}

			delete this.pendingRequests[response.callbackID];
		} else {
			// No pending request with the response's callback ID!
			//
			// Either the callback ID was never sent out in a request, or it
			// was included in a response before. Either way, this shouldn't
			// happen.

			if (response.status.type === 'TIMEOUT') {
				// TIMEOUT indicates a fake response produced by this client
				// itself. This means that a timeout was triggered (possibly
				// very shortly) after an actual response has been received.
				// In this case, we do nothing.
			} else {
				// If we get here, the server has either used a callback ID
				// we did not send it, or it has used the same callback ID
				// in multiple responses (neither of which is covered by the
				// protocol specification).
				var event = new ErrorEvent('error', {
					message: 'Server sent invalid callback ID',
					error: response
				});
				this.dispatchEvent(event);
				//this.close();
			}
		}
	}


	//////////
	// Public API
	//
	public init(serverURL:string, timeoutInterval?:number):Promise<MessageEvent> {
		this.serverURL = serverURL;

		if (timeoutInterval !== undefined) {
			this.timeoutInterval = timeoutInterval;
		}

		return new Promise<MessageEvent>((resolve, reject) => {
			// Save pointers to resolve and reject functions
			this.resolveConnectionPromise = resolve;
			this.rejectConnectionPromise = reject;

			// Open connection
			try {
				this.ws = new WebSocket(this.serverURL);
			} catch (err) {
				throw new WebSocketError("A malformed websocket URL that" +
					" doesn't start with ws:// or wss:// was provided.");
			}

			// Event handlers should be attached before opening the
			// connection, but the WebSocket spec doesn't seem to offer this
			this.ws.addEventListener('error', (event) => {this.wsonerror(event)});
			this.ws.addEventListener('open', (event) => {this.wsonopen(event)});
			this.ws.addEventListener('message', (event) => {this.wsonmessage(event)});
			this.ws.addEventListener('close', (event) => {this.wsonclose(event)});
		});
	};

	// close connection with ws
	public close() {
		this.eventListeners = [];
		this.ws.close();
	}

	////////////////////////////
	// EMU-webApp protocol begins here
	//

	/**
	 * Asynchronously send GETPROTOCOL request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getProtocol() {
		var request = {
			type: 'GETPROTOCOL'
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send GETDOUSERMANAGEMENT request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getDoUserManagement() {
		var request = {
			type: 'GETDOUSERMANAGEMENT'
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send LOGONUSER request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public logOnUser(name, pwd) {
		var request = {
			type: 'LOGONUSER',
			userName: name,
			pwd: pwd
		};
		return this.sendRequest(request);
	};

	/**
	 * Asynchronously send GETGLOBALDBCONFIG request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getDBconfigFile() {
		var request = {
			type: 'GETGLOBALDBCONFIG'
		};
		return this.sendRequest(request);
	};

	/**
	 * Asynchronously send GETBUNDLELIST request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getBundleList() {
		var request = {
			type: 'GETBUNDLELIST'
		};
		return this.sendRequest(request);
	};

	/**
	 * Asynchronously send GETBUNDLE request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getBundle(name, session) {
		var request = {
			type: 'GETBUNDLE',
			name: name,
			session: session
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send SAVEBUNDLE request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public saveBundle(bundleData) {
		var request = {
			type: 'SAVEBUNDLE',
			data: bundleData
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send SAVEDBCONFIG request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public saveConfiguration(configData) {
		var request = {
			type: 'SAVEDBCONFIG',
			data: configData
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send DISCONNECTWARNING request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public disconnectWarning() {
		var request = {
			type: 'DISCONNECTWARNING'
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send GETDOEDITCONFIG request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public getDoEditDBConfig() {
		var request = {
			type: 'GETDOEDITDBCONFIG'
		};
		return this.sendRequest(request);
	}

	/**
	 * Asynchronously send EDITDBCONFIG request to server.
	 * @returns A promise resolving to the server's response.
	 */
	public editDBConfig(subtype, data) {
		var request = {
			type: 'EDITDBCONFIG',
			subtype: subtype,
			data: data
		};
		return this.sendRequest(request);
	}

	//
	// EMU-webApp protocol ends here
	////////////////////////////
}