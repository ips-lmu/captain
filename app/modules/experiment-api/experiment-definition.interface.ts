// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {KeyValuePair} from "../key-value-pair.class";

export interface TrialBlockDefinition {
	type: string;
	repetitions: number;
	stimulusObjects: KeyValuePair<any>[][];
}

export interface ExperimentSection {
	randomise: boolean;
	trialBlocks: TrialBlockDefinition[];
}

export interface ExperimentDefinition {
	uuid: string;
	name: string;
	// @todo discuss versioning information
	version: string;
	sections: ExperimentSection[];
}
