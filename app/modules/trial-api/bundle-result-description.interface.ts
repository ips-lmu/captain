// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Specifies a set of named results from a trial block that will be stored as
 * one bundle in an emuDB.
 */
export interface BundleResultDescription {
	/**
	 * Specifies the name of the bundle itself (the helper function
	 * EmuDBResultService.getNewBundleNamePrefix() helps you construct one)
	 */
	bundleName: string;
	/**
	 * Specifies the name of a trial element that yields an audio signal as its
	 * result.
	 */
	signalElement: string;
	/**
	 * An array that specifies names of trial elements (not their types). The
	 * results of all trial elements identified this way are metadata for
	 * the bundle.
	 */
	bundleAnnotation: string[];
}
