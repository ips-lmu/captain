// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {BundleResultDescription} from "../trial-api/bundle-result-description.interface";
import {KeyValuePair} from "../key-value-pair.class";

export class TestRecorderTrialBlock extends TrialBlock {
	protected csvColumns:string[] = [];
	protected emuDBBundles:BundleResultDescription[] = [];


	public getNextTrial():Promise<TrialElementDefinition<any>[]> {
		console.log(this.currentTrialNumber);
		return super.getNextTrial();
	}

	protected trials:TrialElementDefinition<any>[][] = [
		// First trial
		[{
			type: 'caption',
			name: 'stimulus'
		}, {
			type: 'audioRecorder',
			name: 'recording',
			recordButton: true,
			silenceDetection: 500,
			proceedButton: 'Weiter',
		}],

		// Second trial
		[{
			type: 'caption',
			value: 'Wie gut war die Aufnahme?',
		}, {
			type: 'buttonList',
			value: ['Gut', 'Mittel', 'Schlecht'],
		}],

		// Third trial
		[{
			type: 'audioPlayer',
			autoPlay: true,
			usePreviousResult: 'recording'
		}, {
			type: 'caption',
			value: 'Nochmal: Wie gut war die Aufnahme?'
		}, {
			type: 'buttonList',
			value: ['Gut', 'Mittel', 'Schlecht']
		}]
	];
}
