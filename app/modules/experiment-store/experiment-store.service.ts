// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Injectable} from "angular2/core";

import {ExperimentDefinition} from "../experiment-api/experiment-definition.interface";
import {demoExperiment} from "../../experiments/demo-experiment/experiment-definition";
import {jochimDraxler2016} from "../../experiments/jochim-draxler-2016/experiment-definition";
import {speechRhythmTrainer} from "../../experiments/jochim-draxler-2016/speech-rhythm-trainer";

@Injectable()
export class ExperimentStore {
	constructor() {
		var browserStore = JSON.parse(localStorage.getItem('experimentStore'));

		if (Array.isArray(browserStore)) {
			this.experimentStore = browserStore;
		} else {
			this.experimentStore.push(demoExperiment);
			this.experimentStore.push(jochimDraxler2016);
			this.experimentStore.push(speechRhythmTrainer);

			localStorage.setItem('experimentStore', JSON.stringify(this.experimentStore));
		}
	}

	public getExperimentList():{name:string, uuid:string}[] {
		var list:{name:string, uuid:string}[] = [];

		for (let i = 0; i < this.experimentStore.length; ++i) {
			list.push({
				name: this.experimentStore[i].name,
				uuid: this.experimentStore[i].uuid
			});
		}
		return list;
	}

	public getExperimentByName(name:string):ExperimentDefinition {
		for (let i = 0; i < this.experimentStore.length; ++i) {
			if (this.experimentStore[i].name === name) {
				return this.experimentStore[i];
			}
		}

		return null;
	}

	public getExperimentByUUID(uuid:string):ExperimentDefinition {
		for (var i = 0; i < this.experimentStore.length; ++i) {
			if (this.experimentStore[i].uuid == uuid) {
				return this.experimentStore[i];
			}
		}

		return null;
	}

	public getExperiments():ExperimentDefinition[] {
		return this.experimentStore;
	}

	public addExperiment(definition?:ExperimentDefinition):ExperimentDefinition {
		if (definition) {
			this.experimentStore.push(definition);
			return definition;
		} else {
			var experiment:ExperimentDefinition = {
				name: '',
				uuid: this.newUUID(),
				version: '',
				sections: []
			};
			this.experimentStore.push(experiment);
			return experiment;
		}
	}

	public saveToBrowser():void {
		localStorage.setItem('experimentStore', JSON.stringify(this.experimentStore));
	}

	private newUUID(a?) {
		return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ((1e7).toString() + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, this.newUUID)
	};

	private experimentStore:ExperimentDefinition[] = [];
}
