// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

export interface EmuDBLabel {
	name: string;
	value: string;
}

export interface EmuDBLink {
	fromID: number;
	toID: number;
}

export interface EmuDBItem {
	id: number;
	sampleStart?: number;
	sampleDur?: number;
	samplePoint?: number;
	labels: EmuDBLabel[];
}

export type EmuDBLevelType = "ITEM" | "SEGMENT" | "EVENT";

export interface EmuDBLevel {
	name: string;
	type: EmuDBLevelType;
	items: EmuDBItem[];
}

export interface EmuDBBundleAnnotation {
	name: string;
	annotates: string;
	sampleRate: number;
	levels: EmuDBLevel[];
	links: EmuDBLink[];
}
