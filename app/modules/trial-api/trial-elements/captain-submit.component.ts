// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component} from "angular2/core";
import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

import {CaptainTrialElement} from "../captain-trial-element.class";
import {TrialElementDefinition} from "../trial-element-definition.interface";

@Component({
	selector: 'captain-submit',
	template: `
		<button (click)="finish.emit()">{{element.value}}</button>
		<p class="component-label">submit</p>
	`
})
export class CaptainSubmit extends CaptainTrialElement {
	@Input() element:TrialElementDefinition<string>;
	@Output() result:EventEmitter<string> = new EventEmitter<string>();
	@Output() finish:EventEmitter<void> = new EventEmitter<void>();
}
