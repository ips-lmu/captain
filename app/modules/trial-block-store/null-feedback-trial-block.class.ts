// (c) 2016 Markus Jochim <marksujochim@phonetik.uni-muenchen.de>

import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {KeyValuePair} from "../key-value-pair.class";

import {
	arrayBufferToString,
	decodeAudioFile,
	retrieveFile,
	syllabify,
	TextgridService
} from "browser-signal-processing/browser-signal-processing";
import {EmuDBLevel} from "../result-services/emudb-bundle-annotation.interface";


export class NullFeedbackTrialBlock extends TrialBlock {
	private tgService:TextgridService;

	private originalStimulus:AudioBuffer;
	private originalStimulusSegmentation:EmuDBLevel;
	private recordingSegmentations:EmuDBLevel[] = [];

	private emuDBBundleNamePrefix:string;

	public init(stimulusObject:KeyValuePair<any>[]):Promise<void> {
		super.init(stimulusObject);

		// All bundles produced by this trial block should be grouped
		// together, we therefore request a name prefix from EmuDBResultService.
		this.emuDBBundleNamePrefix = this._emuDBResultService.getNewBundleNamePrefix();

		// Init Textgrid service
		this.tgService = new TextgridService();

		// Find out URL of native utterance
		var pair:KeyValuePair<string> = KeyValuePair.findKey(stimulusObject, 'audioURL');
		if (pair === undefined) {
			return Promise.reject('Stimulus object has no audioURL');
		}
		var url = pair.value;

		//////////
		// Start asynchronous processing
		//
		// 1. Retrieve WAV file defined by URL
		// 2. Parse that file to find its sample rate
		// 3. Feed that file to syllabify() (inclcudes G2P, MAUS, and Pho2Syl)
		// 4. Parse the Textgrid returned by #2
		//

		// The promise returned here is chained to several nested levels
		return retrieveFile(url)
			.then((waveFile:ArrayBuffer) => {
				var sampleRate = 44100; // @todo implement parser function
				// @todo without a parser, make sure this matches the stimulus recordings

				return decodeAudioFile(waveFile, sampleRate)
					.then((buffer:AudioBuffer) => {
						// Save decoded wave file
						this.stimulusObject.push({
							key: 'originalStimulus1',
							value: buffer
						}, {
							key: 'originalStimulus2',
							value: buffer
						}, {
							key: 'originalStimulus3',
							value: buffer
						}, {
							key: 'originalStimulus4',
							value: buffer
						}, {
							key: 'originalStimulus5',
							value: buffer
						});
						this.originalStimulus = buffer;

						return syllabify(waveFile, this.findNamedStimulus('text'), 'deu-DE', 'tg', sampleRate)
							.then((textgrid:ArrayBuffer) => {
								var decodedTextgrid = arrayBufferToString(textgrid);

								return this.tgService.asyncParseTextGrid(decodedTextgrid, sampleRate, "stimulus", url)
									.then((emuBundle) => {
										for (let i = 0; i < emuBundle.levels.length; ++i) {
											if (emuBundle.levels[i].name === 'MAS') {
												this.originalStimulusSegmentation = emuBundle.levels[i];
												return;
											}
										}
										return Promise.reject('Could not' +
											' find MAS tier');
									});
							})
					});
			})
			.catch((reason) => {
				console.debug('init() failed', reason, url);
				return Promise.reject('init() failed');
			});
	}


	protected saveEmuDBResults(saveResults:boolean, trialNumber?:number):Array<{
		name:string,
		annotation:KeyValuePair<any>[],
		signal:AudioBuffer
	}> {
		if (trialNumber === 0) {
			return [];
		}
		
		var stimulusPlaybackCount:number = this.findNamedResult('originalStimulus' + trialNumber);

		this._emuDBResultService.addSimpleBundle(
			this.emuDBBundleNamePrefix + '_recording' + trialNumber,
			[{key: 'stimulusPlaybackCount', value: stimulusPlaybackCount}],
			this.findNamedResult('recording' + trialNumber),
			[this.recordingSegmentations[trialNumber-1]],
			[],
			['MAS']
		);

		if (trialNumber === 1) {
			var promptSegmentation:EmuDBLevel;
			promptSegmentation = this.originalStimulusSegmentation;

			this._emuDBResultService.addSimpleBundle(
				this.emuDBBundleNamePrefix + '_prompt1',
				[],
				this.originalStimulus,
				[promptSegmentation],
				[],
				['MAS']
			);
		}

		return [];
	}

	public getNextTrial():Promise < TrialElementDefinition < any > [] > {
		return super.getNextTrial()
			.then((trial:TrialElementDefinition<any>[]) => {
				// We deliberately do not check for trial === null, because
				// after the last trial, there's still work to do with the
				// stuff produced during that last one.
				if (this.currentTrialNumber <= 1) {
					return trial;
				} else {
					// Load subject's last
					var subjectRecording:AudioBuffer = this.findNamedResult('recording' + (this.currentTrialNumber-1));

					// Feed subject's recording into WebMAUS chain
					return syllabify(subjectRecording, this.findNamedStimulus('text'), 'deu-DE')
						.then((textgrid:ArrayBuffer) => {
							var decodedTextgrid = arrayBufferToString(textgrid);

							return this.tgService.asyncParseTextGrid(decodedTextgrid, subjectRecording.sampleRate, "stimulus", "url")
								.then((emuBundle) => {
									for (let i = 0; i < emuBundle.levels.length; ++i) {
										if (emuBundle.levels[i].name === 'MAS') {
											this.recordingSegmentations.push(emuBundle.levels[i]);
											this.saveEmuDBResults(true, this.currentTrialNumber - 1);

											return trial;
										}
									}
									return Promise.reject('Could not find MAS tier');
								});
						});
				}
			});
	}

	public finishTrial():void {
		this.results.push(KeyValuePair.copyList(this.state));
		this.state = [];

		if (this.currentTrialNumber + 1 === this.trials.length) {
			this.saveCSVResults();
		}
	}

	protected trials:TrialElementDefinition < any > [][] = [
		[{
			type: 'caption',
			value: 'Jetzt kommt der nächste Satz.'
		}, {
			type: 'submit',
			value: 'Weiter'
		}],

		[{
			type: 'audioPlayer',
			name: 'originalStimulus1',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording1',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'originalStimulus2',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording2',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'originalStimulus3',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording3',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'originalStimulus4',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording4',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],


		[{
			type: 'audioPlayer',
			name: 'originalStimulus5',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording5',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}]
	];
}
