import {ExperimentDefinition} from "../../modules/experiment-api/experiment-definition.interface";

export var jochimDraxler2016:ExperimentDefinition = {
	version: '',
	uuid: 'A37E07DC-2277-11E6-BA6E-D76E48CF8D56',
	name: 'jochim-draxler-2016',

	sections: [{
		randomise: false,

		trialBlocks: [{
			type: 'Description',
			repetitions: 1,
			stimulusObjects: [
[{key: 'text', value: 'Willkommen zu dieser Studie!\n\nWir können jetzt beginnen. Sie können jederzeit Fragen stellen!'}, {key: 'button', value: 'Weiter'}]
			]
		}]
	}, {
		randomise: true,
		trialBlocks: [{
			type: 'PsolaFeedback',
			repetitions: 1,

			stimulusObjects: [
[{key:'text', value:'Jetzt sitzen sie beim Frühstück.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-009.wav'}],
[{key:'text', value:'Es ist acht Uhr morgens.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-010.wav'}],
[{key:'text', value:'Vater hat den Tisch gedeckt.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-011.wav'}],
[{key:'text', value:'Günther muss noch einkaufen gehen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-021.wav'}],
[{key:'text', value:'Seine Frau macht ein trauriges Gesicht.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-036.wav'}],
[{key:'text', value:'Du solltest weniger rauchen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-037.wav'}],
[{key:'text', value:'Was macht denn Dein verstauchter Fuß?'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-044.wav'}],
[{key:'text', value:'Zurück geht\'s mit der Bahn.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-056.wav'}],
[{key:'text', value:'Im Topf kocht das Wasser.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-066.wav'}],
[{key:'text', value:'Die drei Männer sind begeistert.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-073.wav'}]
			]
		}, {
			type: 'NullFeedback',
			repetitions: 1,

			stimulusObjects: [
[{key:'text', value:'Die Sonne lacht.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-002.wav'}],
[{key:'text', value:'Gib mir bitte die Butter!'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-018.wav'}],
[{key:'text', value:'Wer möchte noch Milch?'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-019.wav'}],
[{key:'text', value:'Im Geschäft stehen viele Leute.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-026.wav'}],
[{key:'text', value:'Nun schnell nach Hause.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-034.wav'}],
[{key:'text', value:'Gib mir bitte mal die Zeitung!'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-039.wav'}],
[{key:'text', value:'Wir wollen heute spazieren gehen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-046.wav'}],
[{key:'text', value:'Wer trinkt einen Kaffee?'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-052.wav'}],
[{key:'text', value:'Danach tut eine Wanderung gut.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-053.wav'}],
[{key:'text', value:'Die Tante bewohnt ein nettes Häuschen.'}, {key: 'audioURL', value: 'app/experiments/jochim-draxler-2016/be-061.wav'}]
			]
		}]
	}, {
		randomise: false,

		trialBlocks: [{
			type: 'Description',
			repetitions: 1,
			stimulusObjects: [
[{key: 'text', value: 'Fertig!\n\nVielen Dank für Ihre Teilnahme!'}, {key: 'button', value: ':-)'}]
			]
		}]
	}]
};
