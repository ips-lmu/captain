// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {Component} from "angular2/core";
import {Input} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

import {CaptainTrialElement} from "../captain-trial-element.class";
import {TrialElementDefinition} from "../trial-element-definition.interface";

@Component({
	selector: 'captain-slider',
	template: `
		<p>{{element.value}}</p>
		<input #slider type="range" (change)="change(slider.value)">
		<p class="component-label">slider</p>
	`
})
export class CaptainSlider extends CaptainTrialElement{
	@Input() element:TrialElementDefinition<string>;
	@Output() result:EventEmitter<number> = new EventEmitter<number>();

	private change(newValue) {
		this.result.emit(Number(newValue));
	}
}
