// (c) 2016 Markus Jochim <marksujochim@phonetik.uni-muenchen.de>

import {TrialBlock} from "../trial-api/trial-block.class";
import {TrialElementDefinition} from "../trial-api/trial-element-definition.interface";
import {KeyValuePair} from "../key-value-pair.class";

import {
	arrayBufferToString,
	decodeAudioFile,
	psolaAdjustSegmentDurations,
	retrieveFile,
	syllabify,
	TextgridService
} from "browser-signal-processing/browser-signal-processing";
import {EmuDBLevel} from "../result-services/emudb-bundle-annotation.interface";


export class PsolaFeedbackTrialBlock extends TrialBlock {
	private tgService:TextgridService;

	private originalStimulusSegmentation:EmuDBLevel;
	private recordingSegmentations:EmuDBLevel[] = [];

	private promptSignals:AudioBuffer[] = [];

	private emuDBBundleNamePrefix:string;


	/**
	 * The stimulus object for this trial block must include the keys text
	 * and audioURL.
	 *
	 * The file at the specified audioURL is retrieved and then, in
	 * parallel, sent to WebMAUS and stretched by means of PSOLA. It
	 * contains a native utterance that the subject has to imitate.
	 *
	 * The promise this function returns is resolved as soon as both WebMAUS and
	 * PSOLA have finished. It is rejected if one of them fails.
	 *
	 * @param stimulusObject
	 * @returns {Promise}
	 */
	public init(stimulusObject:KeyValuePair<any>[]):Promise<void> {
		super.init(stimulusObject);

		// All bundles produced by this trial block should be grouped
		// together, we therefore request a name prefix from EmuDBResultService.
		this.emuDBBundleNamePrefix = this._emuDBResultService.getNewBundleNamePrefix();

		// Init Textgrid service
		this.tgService = new TextgridService();

		// Find out URL of native utterance
		var pair:KeyValuePair<string> = KeyValuePair.findKey(stimulusObject, 'audioURL');
		if (pair === undefined) {
			return Promise.reject('Stimulus object has no audioURL');
		}
		var url = pair.value;

		//////////
		// Start asynchronous processing
		//
		// 1. Retrieve WAV file defined by URL
		// 2. Parse that file to find its sample rate
		// 3. Feed that file to syllabify() (inclcudes G2P, MAUS, and Pho2Syl)
		// 4. Parse the Textgrid returned by #2
		//

		// The promise returned here is chained to several nested levels
		return retrieveFile(url)
			.then((waveFile:ArrayBuffer) => {
				var sampleRate = 44100; // @todo implement parser function
				// @todo without a parser, make sure this matches the stimulus recordings

				return decodeAudioFile(waveFile, sampleRate)
					.then((buffer:AudioBuffer) => {
						// Save decoded wave file
						this.stimulusObject.push({
							key: 'originalStimulus',
							value: buffer
						});
						this.promptSignals.push(buffer);

						return syllabify(waveFile, this.findNamedStimulus('text'), 'deu-DE', 'tg', sampleRate)
							.then((textgrid:ArrayBuffer) => {
								var decodedTextgrid = arrayBufferToString(textgrid);

								return this.tgService.asyncParseTextGrid(decodedTextgrid, sampleRate, "stimulus", url)
									.then((emuBundle) => {
										for (let i = 0; i < emuBundle.levels.length; ++i) {
											if (emuBundle.levels[i].name === 'MAS') {
												this.originalStimulusSegmentation = emuBundle.levels[i];
												return;
											}
										}
										return Promise.reject('Could not' +
											' find MAS tier');
									});
							})
					});
			})
			.catch((reason) => {
				console.debug('init() failed', reason, url);
				return Promise.reject('init() failed');
			});
	}

	/**
	 * This replaces the vanilla implementation of saveEmuDBResults.
	 *
	 * All this.emuDBBundles[x].bundleAnnotation arrays are empty,
	 * there is therefore no need to try and resolve their values.
	 *
	 * Unlike the vanilla implementation, this one only saves one bundle at
	 * a time rather than all of them in one go. This is because - in this
	 * trial block type - it is called after each trial and not at the end
	 * of the block.
	 *
	 * @param saveResults Is ignored.
	 * @returns An empty array.
	 */
	protected saveEmuDBResults(saveResults:boolean, trialNumber?:number):Array<{
		name:string,
		annotation:KeyValuePair<any>[],
		signal:AudioBuffer
	}> {
		var stimulusPlaybackCount:number;

		if (trialNumber === 0) {
			return [];
		} else if (trialNumber === 1) {
			stimulusPlaybackCount = this.findNamedResult('originalStimulus');
		} else {
			stimulusPlaybackCount = this.findNamedResult('subjectStimulus' + (trialNumber-1));
		}

		this._emuDBResultService.addSimpleBundle(
			this.emuDBBundleNamePrefix + '_recording' + (trialNumber),
			[{key: 'stimulusPlaybackCount', value: stimulusPlaybackCount}],
			this.findNamedResult('recording' + trialNumber),
			[this.recordingSegmentations[trialNumber-1]],
			[],
			['MAS']
		);

		var promptSegmentation:EmuDBLevel;

		if (trialNumber === 1) {
			promptSegmentation = this.originalStimulusSegmentation;
		} else {
			promptSegmentation = {
				name: 'MAS',
				type: 'SEGMENT',
				items: []
			};
		}

		this._emuDBResultService.addSimpleBundle(
			this.emuDBBundleNamePrefix + '_prompt' + trialNumber,
			[],
			this.promptSignals[trialNumber-1],
			[promptSegmentation],
			[],
			['MAS']
		);

		return [];
	}

	/**
	 * This function changes the trials defined in the `trials` array.
	 *
	 * The first trial (trial #0) goes unmodified.
	 *
	 * Starting from the second trial (trial #1), the recording made during
	 * the previous trial needs to be manipulated and injected as stimulus
	 * into the current trial.
	 *
	 * @returns A modified trial with a correct stimulus injected.
	 */
	public getNextTrial():Promise < TrialElementDefinition < any > [] > {
		return super.getNextTrial()
			.then((trial:TrialElementDefinition<any>[]) => {
				// We deliberately do not check for trial === null, because
				// after the last trial, there's still work to do with the
				// stuff produced during that last one.
				if (this.currentTrialNumber <= 1) {
					return trial;
				} else {
					// Load subject's last
					var subjectRecording:AudioBuffer = this.findNamedResult('recording' + (this.currentTrialNumber-1));

					// Feed subject's recording into WebMAUS chain
					return syllabify(subjectRecording, this.findNamedStimulus('text'), 'deu-DE')
						.then((textgrid:ArrayBuffer) => {
							var decodedTextgrid = arrayBufferToString(textgrid);

							return this.tgService.asyncParseTextGrid(decodedTextgrid, subjectRecording.sampleRate, "stimulus", "url")
								.then((emuBundle) => {
									for (let i = 0; i < emuBundle.levels.length; ++i) {
										if (emuBundle.levels[i].name === 'MAS') {
											this.recordingSegmentations.push(emuBundle.levels[i]);
											this.saveEmuDBResults(true, this.currentTrialNumber-1);

											if (trial === null) {
												// In this case, the last trial is already over and we do not need a new stimulus
												return trial;
											}

											return psolaAdjustSegmentDurations(subjectRecording, emuBundle.levels[i].items, this.originalStimulusSegmentation.items)
												.then((manipulatedSignal:AudioBuffer) => {
													this.promptSignals.push(manipulatedSignal);
													trial[0].value = manipulatedSignal;
													return trial;
												});
										}
									}
									return Promise.reject('Could not' +
										' find MAS tier');
								});
						});
				}
			});
	}

	/**
	 * This is basically a copy of the vanilla implementation of finishTrial(),
	 * but it does not saves the emu results after each trial (vanilla saves at
	 * the end
	 * of the trial block.
	 */
	public finishTrial():void {
		this.results.push(KeyValuePair.copyList(this.state));
		this.state = [];

		if (this.currentTrialNumber + 1 === this.trials.length) {
			this.saveCSVResults();
		}
	}

	protected trials:TrialElementDefinition < any > [][] = [
		[{
			type: 'caption',
			value: 'Jetzt kommt der nächste Satz.'
		}, {
			type: 'submit',
			value: 'Weiter'
		}],

		[{
			type: 'audioPlayer',
			name: 'originalStimulus',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording1',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'subjectStimulus1',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording2',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'subjectStimulus2',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording3',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],

		[{
			type: 'audioPlayer',
			name: 'subjectStimulus3',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording4',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}],


		[{
			type: 'audioPlayer',
			name: 'subjectStimulus4',
			text: 'Bitte sprechen Sie diesen Satz nach.',
			autoPlay: true,
			maxRepetitions: 2,
		}, {
			type: 'audioRecorder',
			name: 'recording5',
			recordButton: true,
			silenceDetection: 1000,
			proceedButton: 'Weiter',
		}]
	];
}
